package com.example.utils

import com.example.errors.data.BasicErrors

fun checkEmpty(vararg values:String){
    if (values.any { it.isEmpty() }) throw BasicErrors.EMPTY_FIELDS
}