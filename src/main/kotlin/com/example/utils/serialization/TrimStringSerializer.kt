package com.example.utils.serialization

import io.ktor.serialization.*
import io.ktor.util.reflect.*
import io.ktor.utils.io.*
import io.ktor.utils.io.charsets.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder


@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = String::class)
class TrimStringSerializer : KSerializer<String> {

    override fun deserialize(decoder: Decoder): String {
        return decoder.decodeString().trim()
    }

    override fun serialize(encoder: Encoder, value: String) {
        encoder.encodeString(value.trim())
    }


}