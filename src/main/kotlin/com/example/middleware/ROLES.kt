package com.example.middleware

enum class ROLES  {
    GUEST,STUDENT,TEACHER,ADMIN
}