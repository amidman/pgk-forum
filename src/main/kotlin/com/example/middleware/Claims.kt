package com.example.middleware

object Claims {

    const val ID = "id"

    const val TEACHER_ID = "teacherId"

    const val ROLE = "role"

    const val STUDENT_ID = "studentId"
}