package com.example.middleware

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.algorithms.Algorithm
import com.example.plugins.AuthConfig
import java.util.*

class TokenProvider(
    private val authConfig: AuthConfig,
) {

    private val accessTokenExpires = 3600000L * 24 * 7


    private fun createBaseToken():JWTCreator.Builder{
        return JWT.create()
            .withAudience(authConfig.audience)
            .withIssuer(authConfig.issuer)
            .withExpiresAt(Date(System.currentTimeMillis() + accessTokenExpires))

    }

    fun createStudentToken(userId: Int?,studentId:Int?):String {
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.STUDENT.name)
            .withClaim(Claims.STUDENT_ID,studentId)
            .sign(Algorithm.HMAC256(authConfig.secret))
    }

    fun createAdminToken(userId: Int?,): String {
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.ADMIN.name)
            .sign(Algorithm.HMAC256(authConfig.secret))
    }

    fun createTeacherToken(userId: Int?,teacherId:Int?):String{
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.TEACHER.name)
            .withClaim(Claims.TEACHER_ID,teacherId)
            .sign(Algorithm.HMAC256(authConfig.secret))
    }

    fun createGuestToken(userId: Int?,):String{
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.GUEST.name)
            .sign(Algorithm.HMAC256(authConfig.secret))
    }




}

