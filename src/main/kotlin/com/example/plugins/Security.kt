package com.example.plugins

import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.TokenExpiredException
import com.example.errors.data.TokenErrors
import com.example.middleware.Claims
import com.example.middleware.ROLES
import io.ktor.server.application.*
import org.koin.ktor.ext.get
import org.koin.ktor.ext.inject
import java.util.*
import kotlin.collections.HashMap

data class AuthConfig(
    val secret:String,
    val issuer:String,
    val audience:String,
    val myRealm:String
)



private fun JWTChallengeContext.jwtErrorHandler(verifier:JWTVerifier){
    val jwt = call.request.headers["Authorization"]?.replace("Bearer ","") ?: throw TokenErrors.INVALID_TOKEN
    try {
        verifier.verify(jwt)
    }catch (e:TokenExpiredException){
        throw TokenErrors.EXPIRED
    }
    throw TokenErrors.FORBIDDEN
}
private fun JWTAuthenticationProvider.Config.baseVerifier(config: AuthConfig){
    realm = config.myRealm
    val verifier = JWT
        .require(Algorithm.HMAC256(config.secret))
        .withAudience(config.audience)
        .withIssuer(config.issuer)
        .build()
    verifier(verifier)
    challenge {defaultSheme,realm ->
        jwtErrorHandler(verifier)
    }
}

private fun JWTCredential.baseValidate(role:ROLES?=null):Boolean
=
    payload.getClaim(Claims.ID) != null &&
    payload.getClaim(Claims.ROLE) != null &&
    role?.let { payload.getClaim(Claims.ROLE).asString() == role.name} ?: true

private fun JWTCredential.studentValidate():Boolean = baseValidate(ROLES.STUDENT)
        && payload.getClaim(Claims.STUDENT_ID) != null

private fun JWTCredential.teacherValidate():Boolean = baseValidate(ROLES.TEACHER) && payload.getClaim(Claims.TEACHER_ID) != null

private fun JWTCredential.adminValidate():Boolean = baseValidate(ROLES.ADMIN)

private fun JWTCredential.throwForbidden(role: ROLES) =
    if (payload.getClaim(Claims.ROLE).asString() != role.name) throw TokenErrors.FORBIDDEN else null

private fun JWTAuthenticationProvider.Config.roleValidate(role: ROLES? = null){
    when(role){
        null -> validate { if (it.baseValidate()) JWTPrincipal(it.payload) else null }
        ROLES.STUDENT -> validate { if (it.studentValidate()) JWTPrincipal(it.payload) else null }
        ROLES.TEACHER -> validate { if (it.teacherValidate()) JWTPrincipal(it.payload) else null }
        ROLES.ADMIN -> validate { if (it.adminValidate()) JWTPrincipal(it.payload) else null }
        ROLES.GUEST -> validate { if (it.baseValidate()) JWTPrincipal(it.payload) else null }
    }
}

fun Application.configureSecurity() {

    val config:AuthConfig by inject()

    install(Authentication){
        jwt {
            baseVerifier(config)
            roleValidate()
        }
        ROLES.values().forEach { role->
            jwt(role.name) {
                baseVerifier(config)
                roleValidate(role)
            }
        }
    }
}
