package com.example.plugins

import com.auth0.jwt.exceptions.TokenExpiredException
import com.example.utils.ErrorData
import com.typesafe.config.ConfigException.ValidationFailed
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.requestvalidation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import org.jetbrains.exposed.exceptions.ExposedSQLException
import java.sql.SQLIntegrityConstraintViolationException

fun Application.configureStatusPages(){

    install(StatusPages){

        exception<ResponseException>{call, rExc ->
            call.respond(message = rExc.errorData,status = rExc.statusCode)
        }

        exception<ExposedSQLException>{call, exposedSQLException ->
            val errorData = ErrorData(
                code = "sql error",
                description = exposedSQLException.message.toString()
            )
            call.respond(message = errorData,status = HttpStatusCode.NotFound)
        }

        exception<TokenExpiredException>{call, tokenExpiredException ->
            val errorData = ErrorData(
                code = "token expired",
                description = "Время токена вышло"
            )
            call.respond(message = errorData,status = HttpStatusCode.Unauthorized)
        }

        exception<RequestValidationException>{call, requestValidationException ->
            val errorData = ErrorData(
                code = "validation failed",
                description = requestValidationException.reasons.joinToString()
            )
            call.respond(message = errorData,status = HttpStatusCode.BadRequest)
        }

        exception<IllegalArgumentException>{call, illegalArgumentException ->
            val errorData = ErrorData(
                code = "illegal argument",
                description = illegalArgumentException.message.toString()
            )
            call.respond(message = errorData,status = HttpStatusCode.BadRequest)

        }


    }
}


data class ResponseException(
    val errorData: ErrorData,
    val statusCode: HttpStatusCode = HttpStatusCode.NotFound
):Exception()