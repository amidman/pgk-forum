package com.example.plugins

import com.example.di.configureEntityModules
import com.example.di.configureMiddlewareModules
import io.ktor.server.application.*
import org.koin.ktor.plugin.Koin

fun Application.configureKoinDI(){
    install(Koin){
        configureMiddlewareModules()
        configureEntityModules()
    }
}