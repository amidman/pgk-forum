package com.example.di

import com.example.di.middleware.authModule
import org.koin.core.KoinApplication

fun KoinApplication.configureMiddlewareModules(){
    modules(authModule)
}