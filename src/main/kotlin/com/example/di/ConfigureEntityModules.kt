package com.example.di

import com.example.di.entity.*
import org.koin.core.KoinApplication

fun KoinApplication.configureEntityModules(){
    modules(
        userEntityModule, teacherEntityModule, studentEntityModule,
        groupEntityModule, studentAnswerEntityModule, testEntityModule,
        themeEntityModule,
    )
}