package com.example.di.entity

import com.example.controllers.test.TestController
import com.example.database.entity.question_option.QuestionOptions
import com.example.database.entity.test.Tests
import com.example.database.entity.test_question.TestQuestions
import org.koin.dsl.module

val testEntityModule = module {

    single<TestQuestions>{
        TestQuestions
    }

    single<QuestionOptions>{
        QuestionOptions
    }

    single<Tests> {
        Tests
    }

    single<TestController> {
        TestController(
            questionDao = get(),
            optionDao = get(),
            testDao = get(),
            teacherDao = get(),
            studentDao = get(),
            groupDao = get(),
            themeDao = get(),
        )
    }
}