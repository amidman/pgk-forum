package com.example.di.entity

import com.example.controllers.theme.ThemeController
import com.example.database.entity.theme.Themes
import org.koin.dsl.module

val themeEntityModule = module {

    single<Themes>{
        Themes
    }

    single<ThemeController>{
        ThemeController(
            themeDao = get(),
            teacherDao = get()
        )
    }
}