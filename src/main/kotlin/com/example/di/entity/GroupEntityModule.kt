package com.example.di.entity

import com.example.controllers.group.GroupController
import com.example.database.entity.group.Groups
import com.example.database.entity.teacher_group.TeacherGroups
import org.koin.dsl.module

val groupEntityModule = module {
    single<Groups> {
        Groups
    }

    single<TeacherGroups> {
        TeacherGroups
    }

    single<GroupController> {
        GroupController(
            groupDao = get(),
            teacherGroupDao = get(),
            teacherDao = get(),
            studentDao = get(),
            userDao = get()
        )
    }
}