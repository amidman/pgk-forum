package com.example.di.entity

import com.example.controllers.student.StudentController
import com.example.database.entity.student.Students
import org.koin.dsl.module


val studentEntityModule = module {

    single<Students> {
        Students
    }

    single<StudentController> {
        StudentController(studentDao = get(), groupDao = get(), userDao = get())
    }
}