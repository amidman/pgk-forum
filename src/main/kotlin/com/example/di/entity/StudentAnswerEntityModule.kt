package com.example.di.entity

import com.example.controllers.student_answer.StudentAnswerController
import com.example.database.entity.question_answer.QuestionAnswers
import com.example.database.entity.question_option.QuestionOptions
import com.example.database.entity.student_answers.StudentAnswers
import org.koin.dsl.module

val studentAnswerEntityModule = module {

    single<StudentAnswers> {
        StudentAnswers
    }
    single<QuestionAnswers> {
        QuestionAnswers
    }
    single<QuestionOptions> {
        QuestionOptions
    }

    single<StudentAnswerController> {
        StudentAnswerController(
            studentDao = get(),
            studentAnswerDao = get(),
            questionAnswerDao = get(),
            questionOptionsDao = get(),
            groupDao = get(),
            testDao = get(),
            teacherGroupDao = get()
        )
    }
}