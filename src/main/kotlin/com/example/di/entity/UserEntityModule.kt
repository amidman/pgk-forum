package com.example.di.entity

import com.example.controllers.users.UserController
import com.example.database.entity.user.Users
import com.example.middleware.TokenProvider
import org.koin.dsl.module

val userEntityModule = module {
    single<TokenProvider> {
        TokenProvider(authConfig = get())
    }
    single<Users> {
        Users
    }
    single<UserController> {
        UserController(
            userDao = get(),
            tokenProvider = get(),
            teacherDao = get(),
            studentDao = get()
        )
    }
}