package com.example.di.entity

import com.example.controllers.teacher.TeacherController
import com.example.database.entity.teacher.Teachers
import org.koin.dsl.module


val teacherEntityModule = module {

    single<Teachers> {
        Teachers
    }
    single<TeacherController> {
        TeacherController(
            teacherDao = get(),
            userDao = get(),
            groupDao = get(),
            studentDao = get(),
        )
    }
}