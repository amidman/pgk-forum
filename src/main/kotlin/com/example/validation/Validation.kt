package com.example.validation

import com.example.database.entity.user.UserDTO
import com.example.errors.data.BasicErrors
import com.example.validation.group.groupValidation
import com.example.validation.student.studentValidation
import com.example.validation.test.testValidation
import com.example.validation.theme.themeValidation
import com.example.validation.user.userValidation
import io.ktor.server.application.*
import io.ktor.server.plugins.requestvalidation.*

fun Application.configureValidation(){
    install(RequestValidation){
        groupValidation()
        userValidation()
        studentValidation()
        themeValidation()
        testValidation()
    }
}