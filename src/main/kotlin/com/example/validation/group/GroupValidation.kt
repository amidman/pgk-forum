package com.example.validation.group

import com.example.database.entity.group.GroupDTO
import com.example.database.entity.user.UserDTO
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.groupValidation(){

    validate<GroupDTO> {
        if (it.name.isEmpty())
            return@validate ValidationResult.Invalid("Поле названия группы не должно быть пустым")
        if (it.name.length > 16)
            return@validate ValidationResult.Invalid("Поле названия группы не должно превышать 16 символов")
        if (it.shortName.isNotEmpty() && it.shortName.length > 16)
            return@validate ValidationResult.Invalid("Поле короткого названия не должно превышать 16 символов")
        ValidationResult.Valid
    }

}