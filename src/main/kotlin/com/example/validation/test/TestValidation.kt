package com.example.validation.test

import com.example.models.test.TestInfo
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.testValidation(){
    validate<TestInfo> {test ->
        if (test.themeName.isEmpty() || test.title.isEmpty())
            return@validate ValidationResult.Invalid("Заполните поля названия и темы")
        if (test.title.length > 64)
            return@validate ValidationResult.Invalid("Длина названия не может превышать 64 символа")
        if (test.questions.isEmpty())
            return@validate ValidationResult.Invalid("Вопросы не должны быть пустыми")
        ValidationResult.Valid
    }
}