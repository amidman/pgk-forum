package com.example.validation.user

import com.example.database.entity.user.UserDTO
import com.example.models.auth.LoginData
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.userValidation(){

    validate<LoginData>{
        if(it.login.isEmpty() || it.password.isEmpty())
            return@validate ValidationResult.Invalid("Все поля должны быть заполнены")
        ValidationResult.Valid
    }
    validate<UserDTO>{
        if (it.password.isEmpty() || it.login.isEmpty() || it.fio.isEmpty())
            return@validate ValidationResult.Invalid("Все поля должны быть заполнены")
        if (it.login.length > 16)
            return@validate ValidationResult.Invalid("Поле логина должно быть не более 16 символов")
        if (it.fio.length > 64)
            return@validate ValidationResult.Invalid("Поле ФИО должно быть не болле 64 символов")
        ValidationResult.Valid
    }
}