package com.example.validation.theme

import com.example.database.entity.theme.ThemeDTO
import com.example.models.student.StudentInfoForRegistration
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.themeValidation(){
    validate<ThemeDTO> {
        if (it.themeName.isEmpty())
            return@validate ValidationResult.Invalid("Поле имени не должно быть пустым")
        if (it.themeName.length > 32)
            return@validate ValidationResult.Invalid("Поле названия не может превышать 32 символа")
        if (it.description.length > 1024)
            return@validate ValidationResult.Invalid("Поле описания не может превышать 1024 символа")
        ValidationResult.Valid
    }
}