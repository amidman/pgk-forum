package com.example.validation.student

import com.example.models.student.StudentInfo
import com.example.models.student.StudentInfoForRegistration
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.studentValidation(){
    validate<StudentInfoForRegistration> {
        if (it.password.isEmpty() || it.login.isEmpty() || it.fio.isEmpty())
            return@validate ValidationResult.Invalid("Все поля должны быть заполнены")
        if (it.login.length > 16)
            return@validate ValidationResult.Invalid("Поле логина должно быть не более 16 символов")
        if (it.fio.length > 64)
            return@validate ValidationResult.Invalid("Поле ФИО должно быть не болле 64 символов")
        ValidationResult.Valid
    }
}