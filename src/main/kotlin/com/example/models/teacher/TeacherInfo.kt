package com.example.models.teacher

import com.example.database.entity.teacher.Teachers
import com.example.database.entity.user.Users
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow

@Serializable
data class TeacherInfo(
    val userId:Int? = 0,
    @Serializable(with = TrimStringSerializer::class)
    val fio:String,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val role:String,
    val teacherId:Int,
){
    companion object{
        fun fromResultRow(row:ResultRow):TeacherInfo{
            return TeacherInfo(
                userId = row[Users.id],
                fio = row[Users.fio],
                login = row[Users.login],
                role = row[Users.role],
                teacherId = row[Teachers.id]
            )
        }
    }
}