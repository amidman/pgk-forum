package com.example.models.teacher

import com.example.database.entity.group.GroupDTO
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class TeacherInfoWithGroupList(
    val userId:Int? = 0,
    @Serializable(with = TrimStringSerializer::class)
    val fio:String,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    val role:String,
    val teacherId:Int,
    val groups:List<GroupDTO> = listOf()
){
    companion object{
        fun fromParts(teacherInfo:TeacherInfo,groups: List<GroupDTO>):TeacherInfoWithGroupList{
            return TeacherInfoWithGroupList(
                userId = teacherInfo.userId,
                fio = teacherInfo.fio,
                login = teacherInfo.login,
                role = teacherInfo.role,
                teacherId = teacherInfo.teacherId,
                groups = groups,
            )
        }
    }
}