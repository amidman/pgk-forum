package com.example.models.test

import com.example.database.entity.question_answer.QuestionAnswerDTO
import com.example.database.entity.question_answer.QuestionAnswers
import com.example.database.entity.student_answers.StudentAnswers
import com.example.database.entity.test_question.TestQuestions
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow

@Serializable
data class QuestionAnswer(
    val id:Int? = null,
    val studentAnswerId: Int? = null,
    val questionId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val questionType:String,
    val questionNumber:Int,
    val questionScore:Int,
    @Serializable(with = TrimStringSerializer::class)
    val questionTitle:String,
    @Serializable(with = TrimStringSerializer::class)
    val rightAnswer:String,
    @Serializable(with = TrimStringSerializer::class)
    val studentAnswer: String,
    val questionOptions: List<String>
){
    fun toQuestionAnswerDTO(studentAnswerId: Int):QuestionAnswerDTO = QuestionAnswerDTO(id, studentAnswerId, questionId, studentAnswer)

    companion object {
        fun fromResultRow(studentAnswerId: Int? = null, options:List<String> = listOf(), row: ResultRow):QuestionAnswer{
            return QuestionAnswer(
                id = row[QuestionAnswers.id],
                studentAnswerId = studentAnswerId ?: row[StudentAnswers.id],
                questionId = row[QuestionAnswers.questionId],
                questionType = row[TestQuestions.type],
                questionNumber = row[TestQuestions.number],
                questionScore = row[TestQuestions.score],
                questionTitle = row[TestQuestions.questionTitle],
                rightAnswer = row[TestQuestions.rightAnswer],
                studentAnswer = row[QuestionAnswers.studentAnswer],
                questionOptions = options
            )
        }
    }
}