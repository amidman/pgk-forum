package com.example.models.test

import com.example.database.entity.test_question.TestQuestionDTO
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class Question(
    val id:Int? = null,
    val number:Int,
    val testId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val rightAnswer:String,
    val type:String,
    val score:Int,
    val options:List<String>,
    @Serializable(with = TrimStringSerializer::class)
    val questionTitle:String,
){
    fun testQuestionDTO():TestQuestionDTO = TestQuestionDTO(id, number, testId, rightAnswer, type, score = score, questionTitle = questionTitle)

    fun testQuestionDTO(testId: Int) = TestQuestionDTO(id, number, testId, rightAnswer, type, score=score, questionTitle = questionTitle)

    companion object {
        fun fromTestQuestionDTO(dto:TestQuestionDTO,options: List<String>) =
            Question(
                id = dto.id,
                number = dto.number,
                testId = dto.testId,
                rightAnswer = dto.rightAnswer,
                type = dto.type,
                score = dto.score,
                questionTitle = dto.questionTitle,
                options = options,
            )
    }
}


