package com.example.models.test

import com.example.database.entity.test.TestDTO
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable


@Serializable
data class TestInfo(
    val id:Int? = null,
    val teacherId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val title:String,
    @Serializable(with = TrimStringSerializer::class)
    val themeName:String,
    val maxScore:Int = 0,
    val createdAt: LocalDateTime,
    val questions:List<Question>,
){

    fun testDTO():TestDTO = TestDTO(id, teacherId, title, themeName,createdAt, calcMaxScore())

    fun calcMaxScore():Int{
        return questions.map{it.score}.reduce { acc, i -> acc + i }
    }

    companion object {
        fun fromTestDTO(dto:TestDTO,questions: List<Question>) =
            TestInfo(
                id = dto.id,
                teacherId = dto.teacherId,
                title = dto.title,
                themeName = dto.themeName,
                maxScore = dto.maxScore,
                createdAt = dto.createdAt,
                questions = questions
            )
    }
}

