package com.example.models.test

import com.example.database.entity.student_answers.StudentAnswerDTO
import com.example.database.entity.student_answers.StudentAnswers
import com.example.database.entity.test.Tests
import com.example.errors.data.StudentAnswerErrors
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow
import java.util.*


private fun String.toAnswerString(): String {
    return this.lowercase(Locale.getDefault()).trim()
}

@Serializable
data class StudentAnswer(
    val id: Int? = null,
    val studentId: Int,
    val testId: Int,
    @Serializable(with = TrimStringSerializer::class)
    val testTitle: String,
    val maxScore: Int,
    var totalScore: Int = 0,
    val answerDate: LocalDateTime,
    val questions: List<QuestionAnswer>? = null,
) {
    fun calcTotalScore(): Int {
        var total = 0
        questions?.forEach { questionAnswer ->
            if (questionAnswer.studentAnswer.toAnswerString() == questionAnswer.rightAnswer.toAnswerString())
                total += questionAnswer.questionScore
        } ?: throw StudentAnswerErrors.NO_QUESTIONS
        return total
    }

    fun toStudentAnswerDTO(): StudentAnswerDTO =
        StudentAnswerDTO(
            id = id,
            studentId = studentId,
            testId = testId,
            totalScore = calcTotalScore(),
            answerDate = answerDate
        )

    companion object {
        fun fromResultRow(row: ResultRow): StudentAnswer {
            return StudentAnswer(
                id = row[StudentAnswers.id],
                studentId = row[StudentAnswers.studentId],
                testId = row[StudentAnswers.testId],
                testTitle = row[Tests.title],
                maxScore = row[Tests.maxScore],
                totalScore = row[StudentAnswers.totalScore],
                answerDate = row[StudentAnswers.answerDate].toKotlinLocalDateTime(),
                questions = null
            )
        }

    }
}