package com.example.models.student

import com.example.database.entity.student.Students
import com.example.database.entity.user.Users
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow

@Serializable
data class StudentInfo(
    val userId:Int? = 0,
    @Serializable(with = TrimStringSerializer::class)
    val fio:String,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    val role:String,
    val studentId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String? = null,
){
    companion object{
        fun fromResultRow(row:ResultRow):StudentInfo{
            return StudentInfo(
                userId = row[Users.id],
                fio = row[Users.fio],
                login = row[Users.login],
                role = row[Users.role],
                studentId = row[Students.id],
                groupName = row[Students.groupName]
            )
        }
    }

}
