package com.example.models.student

import com.example.database.entity.student.StudentDTO
import com.example.database.entity.user.UserDTO
import com.example.middleware.ROLES
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class StudentInfoForRegistration(
    @Serializable(with = TrimStringSerializer::class)
    val fio:String,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val password:String,
    val role:String,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String? = null,
){
    fun userDTO() = UserDTO(fio = fio, login = login, password = password, role = ROLES.STUDENT.name)

    fun studentDTO(userId:Int) = StudentDTO(groupName = groupName, userId = userId)
}