package com.example.models.auth

import kotlinx.serialization.Serializable

@Serializable
data class TokenBody(
    val token:String,
    val role:String,
)
