package com.example.models.auth

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class LoginData(
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val password:String,
)
