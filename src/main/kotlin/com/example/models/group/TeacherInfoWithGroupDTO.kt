package com.example.models.group

import com.example.database.entity.group.GroupDTO
import com.example.models.teacher.TeacherInfo
import kotlinx.serialization.Serializable

@Serializable
data class TeacherInfoWithGroupDTO(
    val teacherInfo: TeacherInfo,
    val groupDTO: GroupDTO,
)