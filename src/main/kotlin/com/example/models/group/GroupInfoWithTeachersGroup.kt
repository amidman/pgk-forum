package com.example.models.group

import com.example.models.student.StudentInfo
import com.example.models.teacher.TeacherInfoWithGroupList
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class GroupInfoWithTeachersGroup(
    val id:Int? = null,
    val teachers:List<TeacherInfoWithGroupList>,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String,
    @Serializable(with = TrimStringSerializer::class)
    val groupShortName:String,
    val students:List<StudentInfo>
){
    companion object{
        fun fromParts(groupInfo: GroupInfo,teachers:List<TeacherInfoWithGroupList>) =
            GroupInfoWithTeachersGroup(
                id = groupInfo.id,
                teachers = teachers,
                groupName = groupInfo.groupName,
                groupShortName = groupInfo.groupShortName,
                students = groupInfo.students
            )
    }
}
