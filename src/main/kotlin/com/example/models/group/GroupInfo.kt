package com.example.models.group

import com.example.database.entity.group.GroupDTO
import com.example.models.student.StudentInfo
import com.example.models.teacher.TeacherInfo
import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class GroupInfo(
    val id:Int? = null,
    val teachers:List<TeacherInfo>,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String,
    @Serializable(with = TrimStringSerializer::class)
    val groupShortName:String,
    val students:List<StudentInfo>
){
    companion object {
        fun getFromParts(teacher: List<TeacherInfo>,groupDTO: GroupDTO,students:List<StudentInfo>):GroupInfo{
            return GroupInfo(
                id = groupDTO.id,
                teachers = teacher,
                groupName = groupDTO.name,
                groupShortName = groupDTO.shortName,
                students = students
            )
        }
    }
}

