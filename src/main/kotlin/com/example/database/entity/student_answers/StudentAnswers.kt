package com.example.database.entity.student_answers

import com.example.database.BaseEntity
import com.example.database.entity.student.Students
import com.example.database.entity.test.Tests
import com.example.errors.data.StudentAnswerErrors
import com.example.models.test.StudentAnswer
import kotlinx.coroutines.Deferred
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import java.time.LocalDate
import java.time.LocalDateTime

object StudentAnswers:BaseEntity<StudentAnswerDTO>() {


    // on student delete delete all student answers
    val studentId = reference("studentId",Students.id,onDelete = ReferenceOption.CASCADE)

    // on test delete delete all student answers
    val testId = reference("testId", Tests.id,onDelete = ReferenceOption.CASCADE)

    val totalScore = integer("totalScore")

    val answerDate = datetime("testdate")



    override fun ResultRow.mapToDTO(): StudentAnswerDTO {
        return StudentAnswerDTO(
            id = this[id],
            studentId = this[studentId],
            testId = this[testId],
            totalScore = this[totalScore],
            answerDate = this[answerDate].toKotlinLocalDateTime()
        )
    }

    override fun insertStatement(dto: StudentAnswerDTO): InsertStatement<Number> =
        insert {
            it[studentId] = dto.studentId
            it[testId] = dto.testId
            it[totalScore] = dto.totalScore
            it[answerDate] = dto.answerDate.toJavaLocalDateTime()
        }

    suspend fun checkStudentAnswerNotExist(studentId:Int,testId:Int) = suspendedTransactionAsync{
        select {
            this@StudentAnswers.studentId eq studentId and (this@StudentAnswers.testId eq testId)
        }.mapNotNull { it.mapToDTO() }.singleOrNull()?.let { throw StudentAnswerErrors.EXIST}
    }

    suspend fun getStudentAnswersByStudentId(studentId: Int):Deferred<List<StudentAnswer>> = suspendedTransactionAsync{
        (StudentAnswers innerJoin Tests)
            .select {
                StudentAnswers.studentId eq studentId and(StudentAnswers.testId eq Tests.id)
            }.mapNotNull {
                StudentAnswer.fromResultRow(it)
            }
    }

    suspend fun getStudentAnswerById(studentAnswerId:Int):Deferred<StudentAnswer> = suspendedTransactionAsync {
        (StudentAnswers innerJoin Tests)
            .select { StudentAnswers.testId eq Tests.id and (StudentAnswers.id eq studentAnswerId) }
            .mapNotNull { StudentAnswer.fromResultRow(it) }.singleOrNull() ?: throw StudentAnswerErrors.NOT_FOUND
    }

    suspend fun getStudentAnswer(studentId: Int,testId: Int):Deferred<StudentAnswerDTO?> = suspendedTransactionAsync {
        select{
            (StudentAnswers.testId eq testId) and (StudentAnswers.studentId eq studentId)
        }.mapNotNull {
            it.mapToDTO()
        }.singleOrNull()
    }


    suspend fun changeStudentAnswerById(studentAnswerId:Int,newStudentAnswer:StudentAnswerDTO) = suspendedTransactionAsync {
        update({this@StudentAnswers.id eq studentAnswerId}){
            it[this@StudentAnswers.totalScore] = newStudentAnswer.totalScore
            it[this@StudentAnswers.answerDate] = newStudentAnswer.answerDate.toJavaLocalDateTime()
        }
    }

}