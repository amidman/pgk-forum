package com.example.database.entity.student_answers

import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable


@Serializable
data class StudentAnswerDTO(
    val id:Int? = null,
    val studentId:Int,
    val testId:Int,
    val totalScore:Int,
    val answerDate: LocalDateTime
)