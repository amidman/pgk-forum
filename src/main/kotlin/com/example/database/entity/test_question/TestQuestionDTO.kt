package com.example.database.entity.test_question

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class TestQuestionDTO(
    val id:Int? = null,
    val number:Int,
    val testId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val rightAnswer:String,
    val type:String,
    @Serializable(with = TrimStringSerializer::class)
    val questionTitle:String,
    val score:Int,
)
