package com.example.database.entity.test_question

import com.example.database.BaseEntity
import com.example.database.entity.test.Tests
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object TestQuestions:BaseEntity<TestQuestionDTO>() {

    val number = integer("number")
    val testId = reference("testId", Tests.id,onDelete = ReferenceOption.CASCADE)
    val rightAnswer = varchar("rightAnswer",1024,)
    val type = varchar("type",16)
    val score = integer("score")
    val questionTitle = varchar("questionTitle",2048)


    override fun ResultRow.mapToDTO(): TestQuestionDTO {
        return TestQuestionDTO(
            id = this[id],
            number = this[number],
            testId = this[testId],
            rightAnswer = this[rightAnswer],
            type = this[type],
            score = this[score],
            questionTitle = this[questionTitle]
        )
    }

    override fun insertStatement(dto: TestQuestionDTO): InsertStatement<Number> =
        insert {
            it[testId] = dto.testId
            it[rightAnswer] = dto.rightAnswer
            it[type] = dto.type
            it[score] = dto.score
            it[number] = dto.number
            it[questionTitle] = dto.questionTitle
        }

    suspend fun getTestQuestions(testId:Int):Deferred<List<TestQuestionDTO>> = suspendedTransactionAsync {
        select {
            TestQuestions.testId eq testId
        }.mapNotNull {
            it.mapToDTO()
        }
    }


}