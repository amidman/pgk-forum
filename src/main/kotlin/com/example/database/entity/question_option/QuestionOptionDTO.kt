package com.example.database.entity.question_option

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class QuestionOptionDTO(
    val id:Int? = null,
    val questionId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val value:String,
)
