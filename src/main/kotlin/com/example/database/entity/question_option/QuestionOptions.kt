package com.example.database.entity.question_option

import com.example.database.BaseEntity
import com.example.database.entity.test_question.TestQuestions
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object QuestionOptions:BaseEntity<QuestionOptionDTO>() {

    // on question delete delete all question options
    val questionId = reference("questionId",TestQuestions.id,onDelete = ReferenceOption.CASCADE)

    val value = varchar("value",1024)



    override fun ResultRow.mapToDTO(): QuestionOptionDTO {
        return QuestionOptionDTO(
            id = this[this@QuestionOptions.id],
            value = this[value],
            questionId = this[questionId]
        )
    }

    override fun insertStatement(dto: QuestionOptionDTO): InsertStatement<Number> =
        insert {
            it[questionId] = dto.questionId
            it[value] = dto.value
        }

    suspend fun getQuestionOptions(questionId:Int): Deferred<List<String>> = suspendedTransactionAsync{
        select {
            this@QuestionOptions.questionId eq questionId
        }.mapNotNull { it.mapToDTO().value }
    }

}