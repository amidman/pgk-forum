package com.example.database.entity.teacher

import kotlinx.serialization.Serializable

@Serializable
data class TeacherDTO(
    val id:Int? = null,
    val userId:Int,
)