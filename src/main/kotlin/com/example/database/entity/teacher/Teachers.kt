package com.example.database.entity.teacher

import com.example.database.BaseEntity
import com.example.database.entity.teacher_group.TeacherGroups
import com.example.database.entity.user.Users
import com.example.errors.EntityError
import com.example.errors.data.TeacherErrors
import com.example.errors.data.UserErrors
import com.example.models.teacher.TeacherInfo
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object Teachers:BaseEntity<TeacherDTO>() {

    val userId = reference("userId", Users.id,).uniqueIndex()

    override val entityError: EntityError
        get() = TeacherErrors


    override fun ResultRow.mapToDTO(): TeacherDTO {
        return TeacherDTO(
            id = this[id],
            userId = this[userId]
        )
    }

    override fun insertStatement(dto: TeacherDTO): InsertStatement<Number> =
        insert {
            it[userId] = dto.userId
        }

    suspend fun checkUserNotTeacher(userId:Int) = newSuspendedTransaction{
        select { Teachers.userId eq userId }.singleOrNull()?.let { throw UserErrors.UNIQUE_ID }
    }

    suspend fun findByUserId(userId: Int):Deferred<TeacherInfo> = suspendedTransactionAsync{
        (Teachers innerJoin Users)
            .select{ Teachers.userId eq Users.id and (Users.id eq userId) }
            .mapNotNull {
                TeacherInfo.fromResultRow(it)
            }.singleOrNull() ?: throw TeacherErrors.NOT_FOUND
    }


    suspend fun getAllTeacherInfo(condition:Expression<Boolean>? = null):Deferred<List<TeacherInfo>> = suspendedTransactionAsync{
        condition?.let {
            (Teachers innerJoin Users)
                .select { Teachers.userId eq Users.id and condition }
                .groupBy(Teachers.id)
                .mapNotNull {
                    TeacherInfo.fromResultRow(it)
                }
        }
            ?:
        (Teachers innerJoin Users)
            .select { Teachers.userId eq Users.id }
            .mapNotNull { TeacherInfo.fromResultRow(it) }
    }

    suspend fun getGroupTeachers(groupId:Int):Deferred<List<TeacherInfo>> = suspendedTransactionAsync {
        (TeacherGroups innerJoin Teachers innerJoin Users)
            .select {
                TeacherGroups.groupId eq groupId and(TeacherGroups.teacherId eq Teachers.id) and (Teachers.userId eq Users.id)
            }
            .mapNotNull {
                TeacherInfo.fromResultRow(it)
            }
    }

    suspend fun getTeacher(teacherId:Int):Deferred<TeacherInfo> = suspendedTransactionAsync {
        (Teachers innerJoin Users)
            .select{ Teachers.userId eq Users.id and (Teachers.id eq teacherId) }
            .mapNotNull {
                TeacherInfo.fromResultRow(it)
            }.singleOrNull() ?: throw TeacherErrors.NOT_FOUND
    }



}