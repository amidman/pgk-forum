package com.example.database.entity.group

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class GroupDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val name:String,
    @Serializable(with = TrimStringSerializer::class)
    val shortName:String,
)


