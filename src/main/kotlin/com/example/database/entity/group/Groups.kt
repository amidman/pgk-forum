package com.example.database.entity.group

import com.example.database.BaseEntity
import com.example.database.entity.teacher_group.TeacherGroups
import com.example.errors.EntityError
import com.example.errors.data.GroupErrors
import com.example.errors.data.UserErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object Groups:BaseEntity<GroupDTO>() {

    val name = varchar("name",16).uniqueIndex()

    val shortName = varchar("shortName",16)

    override val entityError: EntityError
        get() = GroupErrors

    override fun ResultRow.mapToDTO(): GroupDTO {
        return GroupDTO(
            id = this[id],
            name = this[name],
            shortName = this[shortName]
        )
    }

    override fun insertStatement(dto: GroupDTO): InsertStatement<Number> =
        insert {
            it[name] = dto.name
            it[shortName] = dto.shortName
        }

    suspend fun checkGroupName(name: String) {
        findGroupByName(name).await()?.let { throw GroupErrors.NAME }
    }

    suspend fun findGroupByName(name: String): Deferred<GroupDTO?> = suspendedTransactionAsync{
        select {
            this@Groups.name eq name
        }.mapNotNull {
            it.mapToDTO()
        }.singleOrNull()
    }

    suspend fun changeName(id:Int,name:String) = suspendedTransactionAsync {
        update({this@Groups.id eq id}) {
            it[this@Groups.name] = name
        }
    }

    suspend fun changeShortName(id:Int,name: String) = suspendedTransactionAsync {
        update({this@Groups.id eq id}) {
            it[this@Groups.shortName] = name
        }
    }

    suspend fun getTeacherGroups(teacherId:Int):Deferred<List<GroupDTO>> = suspendedTransactionAsync{
        (TeacherGroups innerJoin Groups)
            .select {
                TeacherGroups.teacherId eq teacherId and(TeacherGroups.groupId eq Groups.id)
            }
            .mapNotNull {
                it.mapToDTO()
            }
    }




}