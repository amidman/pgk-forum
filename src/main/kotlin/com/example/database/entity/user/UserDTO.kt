package com.example.database.entity.user

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val fio:String,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val password:String,
    val role:String
)
