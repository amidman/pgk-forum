package com.example.database.entity.user

import com.example.database.BaseEntity
import com.example.errors.EntityError
import com.example.errors.data.BasicErrors
import com.example.errors.data.UserErrors
import com.example.middleware.ROLES
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.jetbrains.exposed.sql.update
import java.sql.SQLIntegrityConstraintViolationException

object Users:BaseEntity<UserDTO>() {

    val fio = varchar("fio",64)
    val login = varchar("login",16).uniqueIndex()
    val password = varchar("password",128)
    val role = varchar("role",16)


    override val entityError: EntityError
        get() = UserErrors

    override fun ResultRow.mapToDTO(): UserDTO {
        return UserDTO(
            id = this[id],
            fio = this[fio],
            login = this[login],
            password = this[password],
            role = this[role]
        )
    }

    override fun insertStatement(dto: UserDTO): InsertStatement<Number>  =
        insert {
            it[fio] = dto.fio
            it[login] = dto.login
            it[password] = dto.password
            it[role] = dto.role
        }


    suspend fun checkLogin(login: String) {
        findByLogin(login).await()?.let { throw UserErrors.LOGIN }
    }

    suspend fun findByLogin(login: String):Deferred<UserDTO?> = suspendedTransactionAsync{
        select{
            this@Users.login eq login
        }.mapNotNull {
            it.mapToDTO()
        }.singleOrNull()
    }

    suspend fun changeLogin(entityId:Int,login:String) = suspendedTransactionAsync{
        update({this@Users.id eq entityId}) {
            it[this@Users.login] = login
        }
    }

    suspend fun changeRole(entityId: Int,role:ROLES) = suspendedTransactionAsync {
        update({this@Users.id eq entityId}) {
            it[this@Users.role] = role.name
        }
    }

    suspend fun changePassword(entityId:Int,password:String) = suspendedTransactionAsync{
        update({this@Users.id eq entityId}) {
            it[this@Users.password] = password
        }
    }

    suspend fun changeFio(entityId: Int,fio:String) = suspendedTransactionAsync {
        update({this@Users.id eq entityId}) {
            it[this@Users.fio] = fio
        }
    }




}