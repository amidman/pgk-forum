package com.example.database.entity.question_answer

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class QuestionAnswerDTO(
    val id:Int? = null,
    val studentAnswerId:Int,
    val questionId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val studentAnswer:String,
)
