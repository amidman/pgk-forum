package com.example.database.entity.question_answer

import com.example.database.BaseEntity
import com.example.database.entity.student_answers.StudentAnswers
import com.example.database.entity.test_question.TestQuestions
import com.example.models.test.QuestionAnswer
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object QuestionAnswers : BaseEntity<QuestionAnswerDTO>() {

    //On StudentAnswer delete delete all question answers
    val studentAnswerId = reference("studentAnswerId", StudentAnswers.id, onDelete = ReferenceOption.CASCADE)

    //On question delete delete all question answers
    val questionId = reference("questionId", TestQuestions.id, onDelete = ReferenceOption.CASCADE)

    val studentAnswer = varchar("studentAnswer", 1024)

    override fun ResultRow.mapToDTO(): QuestionAnswerDTO {
        return QuestionAnswerDTO(
            id = this[id],
            studentAnswerId = this[studentAnswerId],
            questionId = this[questionId],
            studentAnswer = this[studentAnswer],
        )
    }

    override fun insertStatement(dto: QuestionAnswerDTO): InsertStatement<Number> =
        insert {
            it[studentAnswerId] = dto.studentAnswerId
            it[questionId] = dto.questionId
            it[studentAnswer] = dto.studentAnswer
        }

    suspend fun getByStudentAnswerId(studentAnswerId: Int): Deferred<List<QuestionAnswer>> = suspendedTransactionAsync {
        (QuestionAnswers innerJoin TestQuestions)
            .select {
                QuestionAnswers.questionId eq TestQuestions.id and (
                        QuestionAnswers.studentAnswerId eq studentAnswerId
                    )
            }
            .mapNotNull {
                QuestionAnswer.fromResultRow(studentAnswerId = studentAnswerId, row = it)
            }
    }

    suspend fun changeStudentAnswer(questionAnswerId: Int, newStudentAnswer: String) = suspendedTransactionAsync {
        update({ this@QuestionAnswers.id eq questionAnswerId }) {
            it[studentAnswer] = newStudentAnswer
        }
    }

    suspend fun deleteByStudentAnswer(studentAnswerId: Int) = suspendedTransactionAsync {
        deleteWhere {
            this@QuestionAnswers.studentAnswerId eq studentAnswerId
        }
    }

}