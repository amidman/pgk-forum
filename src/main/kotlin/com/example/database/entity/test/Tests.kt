package com.example.database.entity.test

import com.example.database.BaseEntity
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.theme.Themes
import kotlinx.coroutines.Deferred
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import java.time.LocalDateTime

object Tests:BaseEntity<TestDTO>() {

    val teacherId = reference("teacherId",Teachers.id)

    val title = varchar("title",64).uniqueIndex()

    val themeName = reference("themeName",Themes.themeName, onUpdate = ReferenceOption.CASCADE)

    val createdAt = datetime("createdAt")

    val maxScore = integer("maxScore").default(0)

    override fun ResultRow.mapToDTO(): TestDTO {
        return TestDTO(
            id = this[Tests.id],
            teacherId = this[Tests.teacherId],
            title = this[Tests.title],
            themeName = this[Tests.themeName],
            maxScore = this[Tests.maxScore],
            createdAt = this[Tests.createdAt].toKotlinLocalDateTime()
        )
    }

    override fun insertStatement(dto: TestDTO): InsertStatement<Number> =
        insert {
            it[teacherId] = dto.teacherId
            it[title] = dto.title
            it[themeName] = dto.themeName
            it[maxScore] = dto.maxScore
            it[createdAt] = dto.createdAt.toJavaLocalDateTime()
        }

    suspend fun getTestsByTeacherId(teacherId:Int):Deferred<List<TestDTO>> = suspendedTransactionAsync{
        select{
            this@Tests.teacherId eq teacherId
        }.mapNotNull { it.mapToDTO() }
    }


    suspend fun changeTestTitle(testId:Int,newTitle:String) = suspendedTransactionAsync {
        update({this@Tests.id eq testId}) {
            it[title] = newTitle
        }
    }

    suspend fun changeTestTheme(testId:Int,newTheme:String) = suspendedTransactionAsync {
        update({this@Tests.id eq testId}) {
            it[themeName] = newTheme
        }
    }
}