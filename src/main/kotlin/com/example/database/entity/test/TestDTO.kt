package com.example.database.entity.test

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable


@Serializable
data class TestDTO(
    val id:Int? = null,
    val teacherId:Int,
    @Serializable(with = TrimStringSerializer::class)
    val title:String,
    @Serializable(with = TrimStringSerializer::class)
    val themeName:String,
    val createdAt:LocalDateTime,
    val maxScore:Int = 0,
)
