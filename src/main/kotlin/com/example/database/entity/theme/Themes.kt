package com.example.database.entity.theme

import com.example.database.BaseEntity
import com.example.database.entity.teacher.Teachers
import com.example.errors.EntityError
import com.example.errors.data.ThemeErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object Themes:BaseEntity<ThemeDTO>() {

    val themeName = varchar("themeName",32).uniqueIndex()

    val description = varchar("description",1024).default("")

    val teacherId = reference("teacherId", Teachers.id,onDelete = ReferenceOption.CASCADE).nullable()

    override val entityError: EntityError
        get() = ThemeErrors

    override fun ResultRow.mapToDTO(): ThemeDTO {
        return ThemeDTO(
            id = this[Themes.id],
            themeName = this[Themes.themeName],
            description = this[Themes.description],
            teacherId = this[Themes.teacherId],
        )
    }

    override fun insertStatement(dto: ThemeDTO): InsertStatement<Number> =
        insert {
            it[themeName] = dto.themeName
            it[description] = dto.description
            it[teacherId] = dto.teacherId
        }

    suspend fun changeName(themeId:Int,newName:String) = suspendedTransactionAsync {
        update({this@Themes.id eq themeId}) {
            it[this@Themes.themeName] = newName
        }
    }

    suspend fun getThemeByName(name: String):Deferred<ThemeDTO> = suspendedTransactionAsync {
        select {this@Themes.themeName eq name}
            .mapNotNull { it.mapToDTO() }
            .singleOrNull() ?: throw ThemeErrors.NOT_FOUND
    }

    suspend fun getThemeByNameNull(name:String):Deferred<ThemeDTO?> = suspendedTransactionAsync {
        select {this@Themes.themeName eq name}
            .mapNotNull { it.mapToDTO() }
            .singleOrNull()
    }

    suspend fun changeDescription(themeId:Int,newDescription:String) = suspendedTransactionAsync {
        update({this@Themes.id eq themeId}) {
            it[this@Themes.description] = newDescription
        }
    }

    suspend fun getTeacherThemes(teacherId:Int):Deferred<List<ThemeDTO>> = suspendedTransactionAsync {
        select { this@Themes.teacherId eq teacherId }
            .mapNotNull { it.mapToDTO() }
    }




}