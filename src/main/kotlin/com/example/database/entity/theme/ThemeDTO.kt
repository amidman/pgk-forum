package com.example.database.entity.theme

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class ThemeDTO(
    val id:Int? = null,
    val teacherId:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val themeName:String,
    @Serializable(with = TrimStringSerializer::class)
    val description:String = "",
)
