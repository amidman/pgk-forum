package com.example.database.entity.teacher_group

import com.example.database.entity.group.GroupDTO
import com.example.database.entity.group.Groups
import com.example.database.entity.group.Groups.mapToDTO
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.user.Users
import com.example.errors.data.TeacherGroupErrors
import com.example.models.teacher.TeacherInfo
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync




object TeacherGroups: Table() {

    /*
        If teacher or group delete delete all teacher groups references on them
    */

    val teacherId = reference("teacherId", Teachers.id,onDelete = ReferenceOption.CASCADE)

    val groupId = reference("groupId", Groups.id,onDelete = ReferenceOption.CASCADE)

    override val primaryKey: PrimaryKey?
        get() = PrimaryKey(teacherId, groupId)

    private fun ResultRow.mapToDTO():TeacherGroupDTO{
        return TeacherGroupDTO(
            teacherId = this[teacherId],
            groupId = this[groupId]
        )
    }

    suspend fun create(teacherId: Int,groupId: Int) = suspendedTransactionAsync {
        insert {
            it[this@TeacherGroups.teacherId] = teacherId
            it[this@TeacherGroups.groupId] = groupId
        }
    }

    suspend fun delete(teacherId: Int,groupId: Int) = suspendedTransactionAsync {
        deleteWhere { (this@TeacherGroups.teacherId eq teacherId) and (this@TeacherGroups.groupId eq groupId) }
    }

    suspend fun deleteWithGroup(groupId: Int) = suspendedTransactionAsync {
        deleteWhere { this@TeacherGroups.groupId eq groupId }
    }

    suspend fun deleteWithTeacher(teacherId: Int) = suspendedTransactionAsync {
        deleteWhere { this@TeacherGroups.teacherId eq teacherId }
    }

    suspend fun getGroupsIdByTeacher(teacherId:Int):Deferred<List<Int>> = suspendedTransactionAsync{
        select { this@TeacherGroups.teacherId eq teacherId }
            .mapNotNull {
                it[groupId]
            }
    }

    suspend fun getTeachersIdByGroup(groupId: Int):Deferred<List<Int>> = suspendedTransactionAsync {
        select { this@TeacherGroups.groupId eq groupId }
            .mapNotNull {
                it[teacherId]
            }
    }

    suspend fun findByPrimaryKey(teacherId: Int,studentId:Int):Deferred<TeacherGroupDTO?> = suspendedTransactionAsync {
        select{
            this@TeacherGroups.teacherId eq teacherId and(this@TeacherGroups.groupId eq studentId)
        }.mapNotNull {
            it.mapToDTO()
        }.singleOrNull()
    }


}