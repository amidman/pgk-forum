package com.example.database.entity.teacher_group

import kotlinx.serialization.Serializable


@Serializable
data class TeacherGroupDTO(
    val teacherId:Int,
    val groupId:Int,
)
