package com.example.database.entity.student

import com.example.utils.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class StudentDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String? = null,
    val userId:Int,
)
