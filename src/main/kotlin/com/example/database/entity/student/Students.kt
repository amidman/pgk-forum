package com.example.database.entity.student

import com.example.database.BaseEntity
import com.example.database.entity.group.Groups
import com.example.database.entity.user.Users
import com.example.errors.EntityError
import com.example.errors.data.StudentErrors
import com.example.errors.data.UserErrors
import com.example.models.student.StudentInfo
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object Students: BaseEntity<StudentDTO>() {

    val groupName = reference("groupName", Groups.name, onDelete = ReferenceOption.SET_NULL, onUpdate = ReferenceOption.CASCADE).nullable()

    val userId = reference("userId", Users.id).uniqueIndex()

    override val entityError: EntityError
        get() = StudentErrors

    override fun ResultRow.mapToDTO(): StudentDTO {
        return StudentDTO(
            id = this[id],
            groupName = this[groupName],
            userId = this[userId],
        )
    }

    override fun insertStatement(dto: StudentDTO): InsertStatement<Number> =
        insert {
            it[groupName] = dto.groupName
            it[userId] = dto.userId
        }

    suspend fun findByUserId(userId: Int): Deferred<StudentInfo> = suspendedTransactionAsync{
        (Students innerJoin Users)
            .select{ Students.userId eq Users.id and(Users.id eq userId)}
            .mapNotNull {
                StudentInfo.fromResultRow(it)
            }.singleOrNull() ?: throw StudentErrors.NOT_FOUND
    }

    suspend fun changeGroup(studentId:Int,groupName:String?) = suspendedTransactionAsync{
        update({Students.id eq studentId}){
            it[this@Students.groupName] = groupName
        }
    }

    suspend fun getStudent(studentId: Int):Deferred<StudentInfo> = suspendedTransactionAsync{
        (Students innerJoin Users)
            .select{ Students.userId eq Users.id and(Students.id eq studentId)}
            .mapNotNull {
                StudentInfo.fromResultRow(it)
            }.singleOrNull() ?: throw StudentErrors.NOT_FOUND
    }

    suspend fun getAllStudentInfo(condition:Expression<Boolean>? = null) = suspendedTransactionAsync{
        condition?.let {
            (Students innerJoin Users)
                .select { Students.userId eq Users.id and it}
                .mapNotNull { StudentInfo.fromResultRow(it) }
        }
            ?:
        (Students innerJoin Users)
            .select { Students.userId eq Users.id}
            .mapNotNull { StudentInfo.fromResultRow(it) }
    }

    suspend fun getAllStudentByGroupName(groupName: String):Deferred<List<StudentInfo>>
        = getAllStudentInfo(
            Students.groupName eq groupName
        )






}