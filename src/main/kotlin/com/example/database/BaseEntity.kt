package com.example.database

import com.example.errors.EntityError
import com.example.errors.data.BasicErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

abstract class BaseEntity<T>: Table() {

    val id = integer("id",).autoIncrement()

    override val primaryKey: PrimaryKey
        get() = PrimaryKey(id)

    open val entityError:EntityError = BasicErrors

    protected abstract fun ResultRow.mapToDTO():T

    protected abstract fun insertStatement(dto: T):InsertStatement<Number>


    suspend fun create(dto:T):Deferred<Int> = suspendedTransactionAsync{
        insertStatement(dto)[this@BaseEntity.id]
    }

    suspend fun delete(entityId:Int) = suspendedTransactionAsync {
        deleteWhere { this@BaseEntity.id eq entityId }
    }

    suspend fun findById(entityId: Int):Deferred<T> = suspendedTransactionAsync {
        select {
            this@BaseEntity.id eq entityId
        }.mapNotNull {
            it.mapToDTO()
        }.singleOrNull() ?: throw entityError.NOT_FOUND
    }


    suspend fun getAll():Deferred<List<T>> = suspendedTransactionAsync{
        selectAll().mapNotNull { it.mapToDTO() }
    }

}