package com.example.database

import com.example.database.entity.group.Groups
import com.example.database.entity.question_answer.QuestionAnswers
import com.example.database.entity.question_option.QuestionOptions
import com.example.database.entity.student.Students
import com.example.database.entity.student_answers.StudentAnswers
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.teacher_group.TeacherGroups
import com.example.database.entity.test.Tests
import com.example.database.entity.test_question.TestQuestions
import com.example.database.entity.theme.Themes
import com.example.database.entity.user.Users
import com.zaxxer.hikari.HikariConfig
import io.ktor.server.config.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.transactionManager
import javax.sql.ConnectionPoolDataSource
import javax.sql.DataSource


class DataBase(
    val config: ApplicationConfig
) {



    fun init(){
        val url = config.property("database.dbUrl").getString()
        val user = config.property("database.user").getString()
        val admin = config.property("database.password").getString()
        val driver = config.property("database.driver").getString()
        val database = Database.connect(
            url = url,
            user = user,
            password = admin,
            driver = driver
        )

        transaction(database) {
            SchemaUtils.createMissingTablesAndColumns(
                Groups,
                QuestionAnswers,
                QuestionOptions,
                Students,
                StudentAnswers,
                Teachers,
                TeacherGroups,
                Tests,
                TestQuestions,
                Themes,
                Users,
            )


        }
    }
}