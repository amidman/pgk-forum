package com.example.routing

import com.example.errors.data.BasicErrors
import com.example.errors.data.TokenErrors
import com.example.middleware.Claims
import com.example.middleware.ROLES
import com.example.routing.group.groupRouting
import com.example.routing.student_answer.studentAnswerRouting
import com.example.routing.teacher.teacherRouting
import com.example.routing.test.testRouting
import com.example.routing.theme.themeRouting
import com.example.routing.user.userRouting
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.util.*
import io.ktor.util.converters.*
import org.koin.ktor.ext.getProperty
import studentRouting


fun ApplicationCall.getUserId():Int{
    val principal = principal<JWTPrincipal>()
    return principal?.payload?.getClaim(Claims.ID)?.asInt() ?: throw TokenErrors.INVALID_TOKEN
}

fun ApplicationCall.getUserRole():ROLES{
    val principal = principal<JWTPrincipal>()
    val role =  principal?.payload?.getClaim(Claims.ROLE)?.asString() ?: throw TokenErrors.INVALID_TOKEN
    return ROLES.valueOf(role)
}

fun ApplicationCall.getTeacherId():Int{
    val principal = principal<JWTPrincipal>()
    return principal?.payload?.getClaim(Claims.TEACHER_ID)?.asInt() ?: throw TokenErrors.INVALID_TOKEN
}

fun ApplicationCall.getStudentId():Int{
    val principal = principal<JWTPrincipal>()
    return principal?.payload?.getClaim(Claims.STUDENT_ID)?.asInt() ?: throw TokenErrors.INVALID_TOKEN
}

fun ApplicationCall.getIntParam(paramName:String):Int{
    return parameters[paramName]?.toInt() ?: throw BasicErrors.BAD_REQUEST
}

fun ApplicationCall.getQueryParam(paramName: String,maxLength:Int? = null):String{
    val paramValue = request.queryParameters[paramName]?.trim() ?: throw BasicErrors.BAD_REQUEST
    if (paramValue.isEmpty())
        throw BasicErrors.EMPTY_FIELDS
    maxLength?.let { len -> if (paramValue.length > len) throw BasicErrors.MANY_SYMBOLS(len) }
    return paramValue
}

fun Application.configureRouting() {

    routing {
        route(Routes.USER_ROUTE){ userRouting() }
        route(Routes.STUDENT_ROUTE){ studentRouting() }
        route(Routes.TEACHER_ROUTE){ teacherRouting() }
        route(Routes.GROUP_ROUTE){ groupRouting() }
        route(Routes.THEME_ROUTE){ themeRouting() }
        route(Routes.TEST_ROUTE){ testRouting() }
        route(Routes.STUDENT_ANSWER_ROUTE){ studentAnswerRouting()}
    }
}
