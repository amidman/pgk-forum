package com.example.routing.student_answer

object StudentAnswerRoutes {
    const val GROUP_ANSWERS = "group-answers"
    const val CREATE = "create"
    const val STUDENT_ANSWERS = "student-answers"
}