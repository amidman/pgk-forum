package com.example.routing.student_answer

import com.example.controllers.student_answer.StudentAnswerController
import com.example.middleware.ROLES
import com.example.models.test.StudentAnswer
import com.example.routing.getQueryParam
import com.example.routing.getStudentId
import com.example.routing.getTeacherId
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

private val groupIdQ = "groupId"
private val studentIdQ = "studentId"
private val testIdQ = "testId"

fun Route.studentAnswerRouting(){

    val studentAnswerController by inject<StudentAnswerController>()

    authenticate(ROLES.TEACHER.name) {
        get(StudentAnswerRoutes.GROUP_ANSWERS){
            val teacherId = call.getTeacherId()
            val testId = call.getQueryParam(studentIdQ).toInt()
            val groupId = call.getQueryParam(groupIdQ).toInt()
            studentAnswerController.checkTeacherAccessToGroup(teacherId,groupId)
            val studentAnswers = studentAnswerController.getStudentsAnswersByGroup(groupId, testId)
            call.respond(studentAnswers)
        }
    }

    authenticate(ROLES.STUDENT.name) {

        post(StudentAnswerRoutes.CREATE) {
            val studentId = call.getStudentId()
            val studentAnswer = call.receive<StudentAnswer>().copy(studentId = studentId)
            val createdStudentAnswer = studentAnswerController.createStudentAnswer(studentId,studentAnswer)
            call.respond(createdStudentAnswer)
        }

        get(StudentAnswerRoutes.STUDENT_ANSWERS){
            val studentId = call.getStudentId()
            val studentAnswer = studentAnswerController.getStudentAnswersByStudentId(studentId)
            call.respond(studentAnswer)
        }

    }
}