package com.example.routing.teacher

object TeacherRoutes {

    const val REGISTER = "register"
    const val INFO = "info"
    const val GET_GROUPS = "get-groups"
    const val GET_GROUPS_BY_ID = "get-groups-by-id"
    const val GET_GROUP_TEACHERS = "{id?}/get-group-teachers"
    const val ALL = "all"
    const val ID = "{id?}"
    const val ALL_WITH_GROUP = "all-with-group"
    const val GET_STUDENT_TEACHERS = "get-student-teachers"
}