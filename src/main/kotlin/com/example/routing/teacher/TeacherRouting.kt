package com.example.routing.teacher

import com.example.controllers.group.GroupController
import com.example.controllers.teacher.TeacherController
import com.example.controllers.test.TestController
import com.example.controllers.users.UserController
import com.example.database.entity.teacher.TeacherDTO
import com.example.database.entity.user.UserDTO
import com.example.middleware.ROLES
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.routing.getStudentId
import com.example.routing.getTeacherId
import com.example.routing.group.GroupRoutes
import com.example.routing.test.TestRoutes
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject


private val teacherIdQ = "teacherId"
private val teacherLoginQ = "teacherLogin"
private val groupNameQ = "groupName"
fun Route.teacherRouting(){

    val userController by inject<UserController>()
    val teacherController by inject<TeacherController>()
    val groupController by inject<GroupController>()
    val testController by inject<TestController>()


    post(TeacherRoutes.REGISTER) {
        val userDTO = call.receive<UserDTO>().copy(role = ROLES.TEACHER.name)
        val userId = userController.registerUser(userDTO)
        val teacherDTO = TeacherDTO(userId = userId)
        val teacherId = teacherController.registerTeacher(teacherDTO)
        val teacherInfo = teacherController.teacherInfo(teacherId)
        call.respond(HttpStatusCode.Created,teacherInfo)
    }

    get(TeacherRoutes.ALL) {
        val teacherInfoList = teacherController.getAllTeachersInfo()
        call.respond(teacherInfoList)
    }

    get(TeacherRoutes.ALL_WITH_GROUP){
        val teacherInfoWithGroupList = teacherController.getAllWithGroup()
        call.respond(teacherInfoWithGroupList)
    }

    get(TeacherRoutes.ID){
        val teacherId = call.getIntParam("id")
        val response = teacherController.teacherInfo(teacherId)
        call.respond(response)
    }

    get(TeacherRoutes.GET_GROUP_TEACHERS) {
        val groupId = call.getIntParam("id")
        val teacherInfoList = teacherController.groupTeachersInfo(groupId)
        call.respond(teacherInfoList)
    }

    authenticate(ROLES.STUDENT.name) {
        get(TeacherRoutes.GET_STUDENT_TEACHERS){
            val studentId = call.getStudentId()
            val teacherInfoList = teacherController.getStudentTeachers(studentId)
            call.respond(teacherInfoList)
        }
    }

    authenticate(ROLES.TEACHER.name) {

        get(TeacherRoutes.INFO) {
            val teacherId = call.getTeacherId()
            val response = teacherController.teacherInfo(teacherId)
            call.respond(response)
        }

        get(TeacherRoutes.GET_GROUPS) {
            val teacherId = call.getTeacherId()
            val groupInfoList = groupController.getGroupsInfoByTeacher(teacherId)
            call.respond(groupInfoList)
        }

    }

    authenticate(ROLES.ADMIN.name) {

        get(TeacherRoutes.GET_GROUPS_BY_ID) {
            val teacherId = call.getQueryParam(teacherIdQ).toInt()
            val groupInfoList = groupController.getGroupsInfoByTeacher(teacherId)
            call.respond(groupInfoList)
        }

        delete(TeacherRoutes.ID) {
            val teacherId = call.getIntParam("id")
            teacherController.deleteTeacher(teacherId)
            call.respond(HttpStatusCode.NoContent)
        }

    }



}