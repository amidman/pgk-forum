package com.example.routing.user

object UserRoutes {

    const val REGISTER = "register"
    const val LOGIN = "login"
    const val INFO = "info"
    const val ALL = "all"
    const val CHANGE_ROLE = "{id?}/change-role"
    const val PASSWORD = "change-password"
    const val CHANGE_LOGIN = "change-login"
    const val CHANGE_FIO = "change-fio"
    const val PASSWORD_BY_ADMIN = "{id?}/change-password"
    const val CHANGE_LOGIN_BY_ADMIN = "{id?}/change-login"
    const val CHANGE_FIO_BY_ADMIN = "{id?}/change-fio"
    const val ID = "{id?}"

}