package com.example.routing.user

import com.example.controllers.users.UserController
import com.example.database.entity.user.UserDTO
import com.example.middleware.ROLES
import com.example.models.auth.LoginData
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.routing.getUserId
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import org.koin.ktor.ext.inject

private val roleQ = "role"
private val pwdQ = "pwd"
private val fioQ = "fio"
private val loginQ = "login"


fun Route.userRouting(){

    val userController = inject<UserController>().value


    get(UserRoutes.ALL) {
        val userDTOList = userController.getAll()
        call.respond(userDTOList)
    }
    get(UserRoutes.ID) {
        val userId = call.getIntParam("id")
        val userDTO = userController.findById(userId)
        call.respond(userDTO)
    }
    post(UserRoutes.REGISTER) {
        val userDTO = call.receive<UserDTO>()
        userController.registerUser(dto = userDTO)
        call.respond(HttpStatusCode.Created,userDTO)
    }

    post(UserRoutes.LOGIN) {
        val loginData = call.receive<LoginData>()
        val tokenBody = userController.login(loginData)
        call.respond(tokenBody)
    }

    authenticate {
        get(UserRoutes.INFO) {
            val userId = call.getUserId()
            val userDTO = userController.findById(userId)
            call.respond(userDTO)
        }
        put(UserRoutes.PASSWORD) {
            val userId = call.getUserId()
            val password = call.getQueryParam(pwdQ)
            userController.changePassword(userId,password)
            call.respond(HttpStatusCode.OK)
        }
        put(UserRoutes.CHANGE_LOGIN) {
            val userId = call.getUserId()
            val login = call.getQueryParam(loginQ,16)
            userController.changeLogin(userId,login)
            call.respond(HttpStatusCode.OK)
        }
        put(UserRoutes.CHANGE_FIO) {
            val userId = call.getUserId()
            val fio = call.getQueryParam(fioQ,64)
            userController.changeFio(userId,fio)
            call.respond(HttpStatusCode.OK)
        }
    }


    authenticate(ROLES.ADMIN.name) {

        put(UserRoutes.PASSWORD_BY_ADMIN) {
            val userId = call.getIntParam("id")
            val password = call.getQueryParam(pwdQ)
            userController.changePassword(userId,password)
            call.respond(HttpStatusCode.OK)
        }
        put(UserRoutes.CHANGE_LOGIN_BY_ADMIN) {
            val userId = call.getIntParam("id")
            val login = call.getQueryParam(loginQ,16)
            userController.changeLogin(userId,login)
            call.respond(HttpStatusCode.OK)
        }
        put(UserRoutes.CHANGE_FIO_BY_ADMIN) {
            val userId = call.getIntParam("id")
            val fio = call.getQueryParam(fioQ,64)
            userController.changeFio(userId,fio)
            call.respond(HttpStatusCode.OK)
        }

        delete(UserRoutes.ID) {
            val userId = call.getIntParam("id")
            userController.delete(userId)
            call.respond(HttpStatusCode.OK)
        }

        put(UserRoutes.CHANGE_ROLE) {
            val userId = call.getIntParam("id")
            val role = call.getQueryParam(roleQ)
            userController.changeUserRole(userId, role)
            call.respond(HttpStatusCode.OK)
        }
    }
}

