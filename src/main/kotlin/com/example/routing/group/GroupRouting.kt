package com.example.routing.group

import com.example.controllers.group.GroupController
import com.example.controllers.student.StudentController
import com.example.controllers.teacher.TeacherController
import com.example.database.entity.group.GroupDTO
import com.example.errors.data.GroupErrors
import com.example.middleware.ROLES
import com.example.models.group.GroupInfoWithTeachersGroup
import com.example.routing.*
import com.example.routing.student.StudentRoutes
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.delay
import org.koin.ktor.ext.inject



private val teacherLoginQ = "teacherLogin"
private val teacherIdQ = "teacherId"
private val groupIdQ = "groupId"
private val groupNameQ = "groupName"
private val shortNameQ = "shortName"

fun Route.groupRouting(){


    val groupController by inject<GroupController>()
    val teacherController by inject<TeacherController>()
    val studentController by inject<StudentController>()

    get(GroupRoutes.ALL){
        val groupInfoList = groupController.getAllGroupInfo()
        call.respond(groupInfoList)
    }

    get(GroupRoutes.ID) {
        val groupId = call.getIntParam("id")
        val groupInfo = groupController.groupInfo(groupId)
        call.respond(groupInfo)
    }


    get(GroupRoutes.GET_BY_NAME){
        val groupName = call.getQueryParam(groupNameQ)
        val groupInfo = groupController.groupInfoByName(groupName)
        call.respond(groupInfo)
    }

    authenticate(ROLES.STUDENT.name) {

        get(GroupRoutes.STUDENT_GROUP) {
            val studentId = call.getStudentId()
            val studentDTO = studentController.findById(studentId)
            val groupInfo = groupController.studentGroupInfo(studentDTO.groupName)
            groupInfo.id ?: throw GroupErrors.NOT_FOUND
            val teacherList = teacherController.getTeachersInfoWithGroup(groupId = groupInfo.id)
            val groupInfoWithTeachersGroup =
                GroupInfoWithTeachersGroup.fromParts(
                    groupInfo,
                    teacherList
                )
            call.respond(groupInfoWithTeachersGroup)
        }

    }

    authenticate(ROLES.TEACHER.name,ROLES.ADMIN.name) {

        post(GroupRoutes.CREATE) {
            val groupDTO = call.receive<GroupDTO>()
            val role = call.getUserRole()
            val groupId =
                if (role == ROLES.TEACHER)
                    groupController.createGroupWithTeacher(call.getTeacherId(), groupDTO)
                else
                    groupController.createGroup(groupDTO)
            call.respond(HttpStatusCode.Created,groupId)
        }
    }
    authenticate(ROLES.ADMIN.name) {
        
        put(GroupRoutes.ADD_TO_TEACHER) {
            val teacherLogin = call.getQueryParam(teacherLoginQ)
            val groupName = call.getQueryParam(groupNameQ)
            val teacherInfoWithGroupDTO = groupController.addGroupToTeacher(teacherLogin=teacherLogin, groupName)
            call.respond(HttpStatusCode.OK,teacherInfoWithGroupDTO)
        }

        delete(GroupRoutes.REMOVE_GROUP_FROM_TEACHER) {
            val teacherLogin = call.getQueryParam(teacherLoginQ)
            val groupId = call.getQueryParam(groupIdQ).toInt()
            groupController.removeGroupFromTeacher(teacherLogin=teacherLogin,groupId=groupId)
            call.respond(HttpStatusCode.NoContent)
        }

        put(GroupRoutes.CHANGE_NAME) {
            val groupId = call.getIntParam("id")
            val groupName = call.getQueryParam(groupNameQ,16)
            groupController.changeGroupName(groupId,groupName)
            call.respond(HttpStatusCode.OK)
        }

        put(GroupRoutes.CHANGE_SHORTNAME) {
            val groupId = call.getIntParam("id")
            val shortName = call.getQueryParam(shortNameQ,16)
            groupController.changeShortName(groupId,shortName)
            call.respond(HttpStatusCode.OK)
        }

        delete(GroupRoutes.ID) {
            val groupId = call.getIntParam("id")
            groupController.delete(groupId)
            call.respond(HttpStatusCode.NoContent)
        }

    }

}