package com.example.routing.group

object GroupRoutes {
    const val ALL = "all"
    const val CREATE = "create"
    const val ID = "{id?}"
    const val ADD_TO_TEACHER = "add-to-teacher"
    const val REMOVE_GROUP_FROM_TEACHER = "remove-from-teacher"
    const val GET_BY_NAME = "get-by-name"
    const val CHANGE_NAME = "{id?}/change-name"
    const val CHANGE_SHORTNAME = "{id?}/change-shortname"
    const val STUDENT_GROUP = "student-group"
    const val ADD_STUDENT_TO_GROUP = "add-student-to-group"
}