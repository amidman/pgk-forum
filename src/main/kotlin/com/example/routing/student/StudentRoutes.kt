package com.example.routing.student

object StudentRoutes {
    const val ALL = "all"
    const val REGISTER = "register"
    const val CHANGE_GROUP = "change-group"
    const val INFO = "info"
    const val ID = "{id?}"
    const val CHANGE_GROUP_BY_STUDENT = "change-group-by-student"
    const val SET_GROUPNAME_NULL = "set-groupname-null"
    const val STUDENT_TEACHERS = "get-teachers"
}
