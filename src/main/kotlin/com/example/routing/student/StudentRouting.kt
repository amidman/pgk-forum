import com.example.routing.student.StudentRoutes
import com.example.controllers.group.GroupController
import com.example.controllers.student.StudentController
import com.example.controllers.teacher.TeacherController
import com.example.controllers.users.UserController
import com.example.database.entity.student.StudentDTO
import com.example.database.entity.student.Students
import com.example.database.entity.user.UserDTO
import com.example.errors.data.GroupErrors
import com.example.middleware.ROLES
import com.example.models.group.GroupInfoWithTeachersGroup
import com.example.models.student.StudentInfoForRegistration
import com.example.routing.*
import com.example.routing.group.GroupRoutes
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject


private val groupNameQ = "groupName"
private val studentLoginQ = "studentLogin"

fun Route.studentRouting(){


    val studentController = inject<StudentController>().value
    val groupController = inject<GroupController>().value
    val userController = inject<UserController>().value
    val teacherController = inject<TeacherController>().value

    get(StudentRoutes.ALL) {
        val studentInfoList = studentController.allStudentInfo()
        call.respond(studentInfoList)
    }

    get(StudentRoutes.ID){
        val studentId = call.getIntParam("id")
        val studentInfo = studentController.studentInfo(studentId)
        call.respond(studentInfo)
    }

    post(StudentRoutes.REGISTER) {
        val registrationInfo = call.receive<StudentInfoForRegistration>()
        registrationInfo.groupName?.let { groupName -> groupController.checkGroup(groupName) }
        val userId = userController.registerUser(registrationInfo.userDTO())
        val studentId = studentController.registerStudent(registrationInfo.studentDTO(userId))
        val studentInfo = studentController.studentInfo(studentId)
        call.respond(studentInfo)
    }


    authenticate(ROLES.STUDENT.name){

        get(StudentRoutes.INFO) {
            val id = call.getStudentId()
            val studentInfo = studentController.studentInfo(studentId = id)
            call.respond(studentInfo)
        }

        put(StudentRoutes.CHANGE_GROUP_BY_STUDENT) {
            val studentId = call.getStudentId()
            val groupName = call.getQueryParam(groupNameQ,16)
            studentController.changeStudentGroup(groupName = groupName,studentId = studentId,)
            call.respond(HttpStatusCode.OK)
        }

        
    }


    authenticate(ROLES.ADMIN.name) {

        put(StudentRoutes.CHANGE_GROUP) {
            val studentLogin = call.getQueryParam(studentLoginQ)
            val groupName = call.getQueryParam(groupNameQ,16)
            val student = studentController.getStudentByLogin(studentLogin)
            val studentInfo = studentController.changeStudentGroup(groupName,student.studentId)
            call.respond(studentInfo)
        }

        put(StudentRoutes.SET_GROUPNAME_NULL) {
            val studentLogin = call.getQueryParam(studentLoginQ)
            val student = studentController.getStudentByLogin(studentLogin)
            val studentInfo = studentController.changeStudentGroup(null,student.studentId)
            call.respond(studentInfo)
        }


        delete(StudentRoutes.ID) {
            val studentId = call.getIntParam("id")
            studentController.deleteStudent(studentId)
            call.respond(HttpStatusCode.NoContent)
        }

    }



}