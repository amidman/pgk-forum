package com.example.routing.theme

import com.example.controllers.theme.ThemeController
import com.example.database.entity.theme.ThemeDTO
import com.example.middleware.ROLES
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.routing.getTeacherId
import com.example.routing.getUserRole
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

private val themeNameQ = "themeName"
private val teacherIdQ = "teacherId"
private val descriptionQ = "description"

fun Route.themeRouting(){

    val themeController = inject<ThemeController>().value

    get(ThemeRoutes.ID) {
        val id = call.getIntParam("id")
        val themeDTO = themeController.findById(id)
        call.respond(themeDTO)
    }

    get(ThemeRoutes.ALL) {
        val themeList = themeController.getAll()
        call.respond(themeList)
    }


    authenticate(ROLES.TEACHER.name,ROLES.ADMIN.name) {

        get(ThemeRoutes.GET_TEACHER_THEMES){
            val role = call.getUserRole()

            val teacherId = if (role == ROLES.TEACHER)
                call.getTeacherId()
            else
                call.getQueryParam(teacherIdQ).toInt()
            val themeDTOList = themeController.getTeacherThemes(teacherId)
            call.respond(themeDTOList)
        }

        post(ThemeRoutes.CREATE) {
            val role = call.getUserRole()
            var themeDTO = call.receive<ThemeDTO>()
            if (role == ROLES.TEACHER)
                themeDTO = themeDTO.copy(teacherId = call.getTeacherId())
            val newTheme = themeController.create(themeDTO)
            call.respond(newTheme)
        }

        delete(ThemeRoutes.ID) {
            val role = call.getUserRole()
            val themeId = call.getIntParam("id")
            if (role == ROLES.TEACHER)
                themeController.deleteByTeacher(call.getTeacherId(), themeId)
            else
                themeController.delete(themeId)
            call.respond(HttpStatusCode.NoContent)
        }

        put(ThemeRoutes.CHANGE_NAME) {
            val role = call.getUserRole()
            val themeId = call.getIntParam("id")
            val newName = call.getQueryParam(themeNameQ,32)
            if (role == ROLES.TEACHER)
                themeController.changeThemeNameByTeacher(teacherId = call.getTeacherId(),themeId,newName)
            else
                themeController.changeThemeName(themeId, newName)
            call.respond(HttpStatusCode.OK)
        }

        put(ThemeRoutes.CHANGE_DESCRIPTION) {
            val role = call.getUserRole()
            val themeId = call.getIntParam("id")
            val newDescription = call.getQueryParam(descriptionQ,1024)
            if (role == ROLES.TEACHER)
                themeController.changeThemeDescriptionByTeacher(teacherId = call.getTeacherId(), newDescription = newDescription, themeId = themeId)
            else
                themeController.changeThemeDescription(themeId, newDescription)
            call.respond(HttpStatusCode.OK)
        }

    }

}