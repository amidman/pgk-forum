package com.example.routing.theme

object ThemeRoutes {
    const val ALL = "all"
    const val CREATE = "create"
    const val ID = "{id?}"
    const val CHANGE_NAME = "{id?}/change-name"
    const val CHANGE_DESCRIPTION = "{id?}/change-description"
    const val GET_TEACHER_THEMES = "get-teacher-themes"
}