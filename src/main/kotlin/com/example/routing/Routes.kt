package com.example.routing

object Routes {

    const val USER_ROUTE = "/users/"
    const val STUDENT_ROUTE = "/students/"
    const val TEACHER_ROUTE = "/teachers/"
    const val GROUP_ROUTE = "/groups/"
    const val THEME_ROUTE = "/themes/"
    const val TEST_ROUTE = "/tests/"
    const val STUDENT_ANSWER_ROUTE = "/student-answers/"

}