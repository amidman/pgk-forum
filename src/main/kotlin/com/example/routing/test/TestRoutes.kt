package com.example.routing.test

object TestRoutes {

    const val GET_TEACHER_TESTS = "get-teacher-tests"
    const val CREATE = "create"
    const val DELETE = "delete"
    const val CHANGE_THEME = "change-theme"
    const val CHANGE_TITLE = "change-title"
    const val GET_TESTS_BY_TEACHER_ID = "get-tests-by-teacher-id"
}