package com.example.routing.test

import com.example.controllers.test.TestController
import com.example.middleware.ROLES
import com.example.models.test.TestInfo
import com.example.routing.getQueryParam
import com.example.routing.getTeacherId
import com.example.routing.teacher.TeacherRoutes
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject



private val testIdQ = "testId"
private val themeNameQ = "themeName"
private val themeTitleQ = "themeTitle"
private val teacherIdQ = "teacherId"

fun Route.testRouting(){

    val testController by inject<TestController>()


    get(TestRoutes.GET_TESTS_BY_TEACHER_ID){
        val teacherId = call.getQueryParam(teacherIdQ).toInt()
        val testInfoList = testController.getTeacherTests(teacherId)
        call.respond(testInfoList)
    }

    authenticate(ROLES.TEACHER.name) {
        get(TestRoutes.GET_TEACHER_TESTS){
            val teacherId = call.getTeacherId()
            val testInfoList = testController.getTeacherTests(teacherId)
            call.respond(testInfoList)
        }

        put(TestRoutes.CHANGE_THEME) {
            val testId = call.getQueryParam(testIdQ).toInt()
            val themeName = call.getQueryParam(themeNameQ)
            testController.changeTestTheme(testId,themeName)
            call.respond(HttpStatusCode.NoContent)
        }

        put(TestRoutes.CHANGE_TITLE) {
            val testId = call.getQueryParam(testIdQ).toInt()
            val themeName = call.getQueryParam(themeTitleQ,64)
            testController.changeTestTheme(testId,themeName)
            call.respond(HttpStatusCode.NoContent)
        }

        post(TestRoutes.CREATE) {
            val teacherId = call.getTeacherId()
            val testInfo = call.receive<TestInfo>().copy(teacherId = teacherId)
            val testInfoResponse = testController.createTest(testInfo)
            call.respond(testInfoResponse)
        }

        delete(TestRoutes.DELETE) {
            val testId = call.getQueryParam(testIdQ).toInt()
            testController.delete(testId)
            call.respond(HttpStatusCode.NoContent)
        }


    }

}