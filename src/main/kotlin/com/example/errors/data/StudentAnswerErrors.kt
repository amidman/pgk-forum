package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object StudentAnswerErrors:EntityError {
    override val NOT_FOUND: ResponseException = ResponseException(
        ErrorData(
            code="student answer not found",
            description = "Работа студента по данному тесту не найдена"
        ),
        HttpStatusCode.NotFound
    )

    val EXIST = ResponseException(
        ErrorData(
            code = "already exist",
            description = "Вы уже проходили данный тест, если вы желаете пройти его повторно отправьте запрос преподователю или попросите его сделать это лично"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_QUESTION_DATA = ResponseException(
        ErrorData(
            "wrong question data",
            "Количество ответов не совпадает с количеством вопросов теста"
        ),
        HttpStatusCode.BadRequest
    )

    val NO_QUESTIONS = ResponseException(
        ErrorData(
            "no questions",
            "В ответе отсутсвуют вопросы"
        ),
        HttpStatusCode.BadRequest
    )


}