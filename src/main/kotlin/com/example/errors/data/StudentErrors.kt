package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object StudentErrors:EntityError {

    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = "user not found",
            description = "Студент не найден"
        ),
        HttpStatusCode.NotFound
    )

    val GROUP_NULL = ResponseException(
        ErrorData(
            code = "student group null",
            description = "У студента нет группы"
        ),
        HttpStatusCode.NotFound
    )

    val STUDENT_ALREADY_IN_GROUP = ResponseException(
        ErrorData(
            code = "student already in that group",
            description = "Студент уже состоит в данной группе"
        ),
        HttpStatusCode.BadRequest
    )

}