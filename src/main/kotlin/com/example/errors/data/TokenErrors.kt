package com.example.errors.data

import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*


object TokenErrors {


    val INVALID_TOKEN = ResponseException(
        ErrorData(
            code = "invalid",
            description = "Аунтефикация не удалась"
        ),
        HttpStatusCode(421,"Неверный токен доступа")
    )

    val FORBIDDEN = ResponseException(
        ErrorData(
            code = "forbidden",
            description = "Ошибка доступа"
        ),
        HttpStatusCode.Forbidden
    )

    val EXPIRED = ResponseException(
        ErrorData(
            code = "token expired",
            description = "Время действия токена вышло"
        ),
        HttpStatusCode.Unauthorized
    )

}