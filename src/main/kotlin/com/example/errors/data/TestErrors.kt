package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object TestErrors:EntityError {
    override val NOT_FOUND: ResponseException = ResponseException(
        ErrorData(
            "test not found",
            "Тест не найден",
        ),
        HttpStatusCode.NotFound
    )



}