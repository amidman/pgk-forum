package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object BasicErrors:EntityError {
    private const val not_found_code = "not found"
    private const val empty_fields = "emoty fields"
    private const val bad_request = "bad request"
    private const val wrong_date = "wrong date"
    private const val many_symbols = "many symbols"
    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = not_found_code,
            description = "По вашему запросу ничего не найдено"
        ),
        HttpStatusCode.NotFound
    )

    val EMPTY_FIELDS = ResponseException(
        ErrorData(
            code = empty_fields,
            description = "Пожалуйста заполните все поля"
        ),
        HttpStatusCode.BadRequest
    )

    val BAD_REQUEST = ResponseException(
        ErrorData(
            code = bad_request,
            description = "Проверьте правильность введённых данных и повторите попытку"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_DATE_FORMAT = ResponseException(
        ErrorData(
            code = wrong_date,
            description = "Неправильный формат даты"
        ),
        HttpStatusCode.BadRequest
    )

    fun MANY_SYMBOLS(size:Int) = ResponseException(
        ErrorData(
            code = many_symbols,
            description = "Длина этого поля ограничена $size символами"
        ),
        HttpStatusCode.BadRequest,
    )


}