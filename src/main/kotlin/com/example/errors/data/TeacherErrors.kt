package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object TeacherErrors:EntityError {

    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = "teacher not found",
            description = "Учитель не найден"
        ),
        HttpStatusCode.NotFound
    )
}