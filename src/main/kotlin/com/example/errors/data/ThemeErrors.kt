package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object ThemeErrors:EntityError {
    override val NOT_FOUND: ResponseException = ResponseException(
        ErrorData(
            code = "theme not found",
            description = "Тема не найдена"
        ),
        HttpStatusCode.NotFound
    )

    val NAME = ResponseException(
        ErrorData(
            code = "wrong theme name",
            description = "Тема с таким названием уже существует"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_TEACHER = ResponseException(
        ErrorData(
            code = "wrong theme teacher",
            description = "Вы не можете совершать действие с чужой темой"
        ),
        HttpStatusCode.BadRequest
    )
}