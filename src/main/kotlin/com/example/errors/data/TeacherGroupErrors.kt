package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object TeacherGroupErrors:EntityError {

    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = "teacher group not found",
            description = "У данного учителя и группы нет связи"
        ),
        HttpStatusCode.BadRequest
    )

    val EXIST = ResponseException(
        ErrorData(
            code = "teacher group already exist",
            description = "Данная группа уже принадлежит учителю"
        ),
        HttpStatusCode.BadRequest
    )
}