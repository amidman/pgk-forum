package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object GroupErrors:EntityError {

    val NAME = ResponseException(
        ErrorData(
            code="group name",
            description = "Группа с таким именем уже существует"
        ),
        HttpStatusCode.BadRequest
    )

   override val NOT_FOUND = ResponseException(
        ErrorData(
            code = "group not found",
            description = "Группа не найдена"
        ),
        HttpStatusCode.NotFound
    )
}