package com.example.errors.data

import com.example.errors.EntityError
import com.example.plugins.ResponseException
import com.example.utils.ErrorData
import io.ktor.http.*

object UserErrors:EntityError {

    val LOGIN = ResponseException(
        ErrorData(
            code = "login",
            description = "Пользователь с данным логином уже существует"
        ),
        HttpStatusCode.BadRequest
    )

    val PASSWORD = ResponseException(
        ErrorData(
            code = "password",
            description = "Неверный пароль"
        )
    )

    val UNIQUE_ID = ResponseException(
        ErrorData(
            code = "unique_id",
            description = "Данный пользователь уже зарегистрирован как пользователь с ролью Ученик/Учитель"
        ),
        HttpStatusCode.BadRequest
    )

    val ROLE = ResponseException(
        ErrorData(
            code = "role",
            description = "Указанной роли не существует"
        ),
        HttpStatusCode.BadRequest
    )

    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = "user not found",
            description = "Пользователь не найден"
        ),
        HttpStatusCode.NotFound
    )
}