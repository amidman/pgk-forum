package com.example.errors

import com.example.plugins.ResponseException

interface EntityError {
    val NOT_FOUND:ResponseException
}