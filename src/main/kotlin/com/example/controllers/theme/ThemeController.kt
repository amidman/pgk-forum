package com.example.controllers.theme

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.theme.ThemeDTO
import com.example.database.entity.theme.Themes
import com.example.errors.data.ThemeErrors

class ThemeController(
    private val themeDao:Themes,
    private val teacherDao:Teachers
): BaseController<ThemeDTO>() {
    override val dao: BaseEntity<ThemeDTO> = themeDao


    private suspend fun checkTheme(themeId: Int){
        findById(themeId)
    }

    private suspend fun checkTeacher(teacherId:Int){
        teacherDao.findById(teacherId).await()
    }

    private suspend fun checkThemeNameUnique(themeName:String){
        themeDao.getThemeByNameNull(themeName).await()?.let { throw ThemeErrors.NAME }
    }

    private suspend fun checkTeacherAccess(teacherId: Int,themeId:Int){
        checkTeacher(teacherId)
        val themeDTO = findById(themeId)
        themeDTO.teacherId?.let { tId ->
            if (teacherId != tId)
                throw ThemeErrors.WRONG_TEACHER
        } ?: throw ThemeErrors.WRONG_TEACHER
    }

    suspend fun create(themeDTO: ThemeDTO):ThemeDTO{
        themeDTO.teacherId?.let { checkTeacher(teacherId = it) }
        checkThemeNameUnique(themeDTO.themeName)
        val id = themeDao.create(themeDTO).await()
        println(id)
        return themeDTO.copy(id = id)
    }

    suspend fun changeThemeName(themeId:Int,newName:String){
        checkTheme(themeId)
        checkThemeNameUnique(newName)
        themeDao.changeName(themeId,newName).await()
    }

    suspend fun changeThemeNameByTeacher(teacherId: Int,themeId: Int,newName: String){
        checkTeacherAccess(teacherId, themeId)
        changeThemeName(themeId, newName)
    }

    suspend fun changeThemeDescription(themeId: Int,newDescription:String){
        checkTheme(themeId)
        themeDao.changeDescription(themeId, newDescription).await()
    }

    suspend fun changeThemeDescriptionByTeacher(themeId: Int,teacherId: Int,newDescription: String){
        checkTeacherAccess(teacherId, themeId)
        changeThemeDescription(themeId, newDescription)
    }

    suspend fun getTeacherThemes(teacherId: Int): List<ThemeDTO> {
        checkTeacher(teacherId)
        return themeDao.getTeacherThemes(teacherId).await()
    }

    suspend fun deleteByTeacher(teacherId:Int,themeId: Int){
        checkTeacherAccess(teacherId, themeId)
        delete(themeId)
    }
}