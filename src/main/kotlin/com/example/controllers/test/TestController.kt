package com.example.controllers.test

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.group.Groups
import com.example.database.entity.question_option.QuestionOptionDTO
import com.example.database.entity.question_option.QuestionOptions
import com.example.database.entity.student.Students
import com.example.database.entity.teacher.TeacherDTO
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.test.TestDTO
import com.example.database.entity.test.Tests
import com.example.database.entity.test_question.TestQuestions
import com.example.database.entity.theme.Themes
import com.example.errors.data.BasicErrors
import com.example.errors.data.GroupErrors
import com.example.errors.data.StudentErrors
import com.example.models.test.Question
import com.example.models.test.TestInfo
import kotlinx.coroutines.*

class TestController(
    private val questionDao:TestQuestions,
    private val optionDao:QuestionOptions,
    private val testDao:Tests,
    private val teacherDao:Teachers,
    private val studentDao: Students,
    private val groupDao:Groups,
    private val themeDao:Themes
):BaseController<TestDTO>(){

    override val dao: BaseEntity<TestDTO> = testDao

    private suspend fun checkTeacher(teacherId: Int){
        teacherDao.findById(teacherId).await()
    }

    private suspend fun checkTest(testId:Int){
        findById(testId)
    }

    private suspend fun checkThemeExist(themeName:String){
        themeDao.getThemeByName(themeName).await()
    }

    private fun CoroutineScope.createOptions(questionId:Int,options:List<String>) {
        options.forEach { value->
            launch{
                val optionDTO = QuestionOptionDTO(id = null,questionId,value)
                optionDao.create(optionDTO).await()
            }
        }
    }

    private suspend fun getQuestions(testId:Int):List<Question>{
        val questionDTOs = questionDao.getTestQuestions(testId).await()
        return withContext(Dispatchers.Default) {
            questionDTOs.map { dto ->
                async {
                    val options = optionDao.getQuestionOptions(dto.id ?: throw BasicErrors.NOT_FOUND)
                    Question.fromTestQuestionDTO(dto, options.await())
                }
            }.map { it.await() }
        }
    }

    suspend fun createTest(testInfo:TestInfo):TestInfo{
        checkTeacher(testInfo.teacherId)
        checkThemeExist(testInfo.themeName)
        val testDTO = testInfo.testDTO()
        val testId = testDao.create(testDTO).await()
        withContext(Dispatchers.IO){
            testInfo.questions.forEach { quest->
                launch {
                    val questionDTO = quest.testQuestionDTO(testId)
                    println(questionDTO)
                    val questionId = questionDao.create(questionDTO).await()
                    createOptions(questionId,quest.options)
                }
            }
        }
        return findById(testId).toTestInfoAsync()
    }

    suspend fun TestDTO.toTestInfoAsync():TestInfo {
        val questions = getQuestions(this.id ?: throw BasicErrors.NOT_FOUND)
        return TestInfo.fromTestDTO(this, questions)
    }

    suspend fun getTeacherTests(teacherId:Int):List<TestInfo>{
        checkTeacher(teacherId)
        val testDTOs = testDao.getTestsByTeacherId(teacherId).await()
        return withContext(Dispatchers.Default){
            testDTOs.map {dto ->
                async {
                    dto.toTestInfoAsync()
                }
            }.map { it.await() }
        }
    }

    suspend fun getTestListByStudentId(studentId:Int):List<TestInfo>{
        val studentGroupName = studentDao.findById(studentId).await().groupName ?: throw StudentErrors.GROUP_NULL
        val groupId = groupDao.findGroupByName(studentGroupName).await()?.id ?: throw GroupErrors.NOT_FOUND
        val teachers = teacherDao.getGroupTeachers(groupId = groupId).await()
        return withContext(Dispatchers.Default){
            teachers.map{ teacherInfo ->
                async { getTeacherTests(teacherInfo.teacherId) }
            }.map { it.await() }.reduce{ a,b -> a.plus(b) }
        }
    }

    suspend fun changeTestTitle(testId:Int,newTitle:String){
        checkTest(testId)
        testDao.changeTestTitle(testId,newTitle).await()
    }

    suspend fun changeTestTheme(testId: Int,newTheme:String){
        checkTest(testId)
        checkThemeExist(newTheme)
        testDao.changeTestTheme(testId, newTheme).await()
    }


}



