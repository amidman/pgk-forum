package com.example.controllers

import com.example.database.BaseEntity

abstract class BaseController<T> {
    internal abstract val dao:BaseEntity<T>


    suspend fun getAll():List<T>{
        return dao.getAll().await()
    }

    suspend fun findById(entityId:Int):T{
        return dao.findById(entityId).await()
    }

    suspend fun delete(entityId: Int){
        dao.findById(entityId).await()
        dao.delete(entityId).await()
    }


}