package com.example.controllers.student

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.group.GroupDTO
import com.example.database.entity.group.Groups
import com.example.database.entity.student.StudentDTO
import com.example.database.entity.student.Students
import com.example.database.entity.user.UserDTO
import com.example.database.entity.user.Users
import com.example.errors.data.GroupErrors
import com.example.errors.data.StudentErrors
import com.example.middleware.ROLES
import com.example.models.student.StudentInfo
import com.example.models.student.StudentInfoForRegistration

class StudentController(
    private val studentDao:Students,
    private val groupDao:Groups,
    private val userDao:Users,
): BaseController<StudentDTO>() {

    override val dao: BaseEntity<StudentDTO> = studentDao

    suspend fun registerStudent(dto:StudentDTO):Int{
        dto.groupName?.let { groupDao.findGroupByName(it).await() }
        return studentDao.create(dto).await()
    }

    private suspend fun checkUser(userId: Int){
        userDao.findById(userId).await()
    }

    private suspend fun checkStudent(studentId: Int){
        studentDao.findById(studentId).await()
    }

    suspend fun checkGroup(groupName: String){
        groupDao.findGroupByName(groupName).await() ?: throw GroupErrors.NOT_FOUND
    }

    suspend fun studentInfo(studentId: Int):StudentInfo{
        checkStudent(studentId)
        return studentDao.getStudent(studentId).await()
    }

    suspend fun allStudentInfo():List<StudentInfo>{
        return studentDao.getAllStudentInfo().await()
    }

    suspend fun getAllStudentInfoByGroupName(groupName: String):List<StudentInfo>{
        checkGroup(groupName)
        return studentDao.getAllStudentByGroupName(groupName).await()
    }

    suspend fun changeStudentGroup(groupName:String?, studentId: Int):StudentInfo{
        groupName?.let { checkGroup(groupName)}
        val studentInfo = studentDao.getStudent(studentId).await()
        if (studentInfo.groupName != null && studentInfo.groupName == groupName)
            throw StudentErrors.STUDENT_ALREADY_IN_GROUP
        studentDao.changeGroup(studentId, groupName).await()
        return studentDao.getStudent(studentId).await()
    }

    suspend fun deleteStudent(studentId: Int){
        val student = findById(studentId)
        userDao.changeRole(student.userId, ROLES.GUEST).await()
        studentDao.delete(studentId).await()
    }

    suspend fun getStudentByLogin(studentLogin:String):StudentInfo{
        val userId = userDao.findByLogin(studentLogin).await()?.id ?: throw StudentErrors.NOT_FOUND
        return studentDao.findByUserId(userId).await()
    }


}