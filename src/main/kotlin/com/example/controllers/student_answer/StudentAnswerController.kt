package com.example.controllers.student_answer

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.group.Groups
import com.example.database.entity.question_answer.QuestionAnswers
import com.example.database.entity.question_option.QuestionOptions
import com.example.database.entity.student.Students
import com.example.database.entity.student_answers.StudentAnswerDTO
import com.example.database.entity.student_answers.StudentAnswers
import com.example.database.entity.teacher_group.TeacherGroups
import com.example.database.entity.test.Tests
import com.example.errors.data.StudentAnswerErrors
import com.example.models.test.QuestionAnswer
import com.example.models.test.StudentAnswer
import kotlinx.coroutines.*

class StudentAnswerController(
    private val studentDao: Students,
    private val studentAnswerDao: StudentAnswers,
    private val questionAnswerDao: QuestionAnswers,
    private val questionOptionsDao: QuestionOptions,
    private val groupDao: Groups,
    private val testDao: Tests,
    private val teacherGroupDao: TeacherGroups,
) : BaseController<StudentAnswerDTO>() {

    override val dao: BaseEntity<StudentAnswerDTO> = studentAnswerDao


    private suspend fun checkStudent(studentId: Int) {
        studentDao.findById(studentId).await()
    }

    private suspend fun getOptions(questionId: Int): List<String> {
        return questionOptionsDao.getQuestionOptions(questionId).await()
    }

    private suspend fun checkTest(testId: Int) {
        testDao.findById(testId).await()
    }


    private suspend fun getQuestions(studentAnswerId: Int): List<QuestionAnswer> {
        val questionAnswers = questionAnswerDao.getByStudentAnswerId(studentAnswerId).await()

        return withContext(Dispatchers.Default) {
            questionAnswers.map {
                async {
                    it.copy(questionOptions = getOptions(it.questionId))
                }
            }.map { it.await() }
        }
    }

    private suspend fun List<StudentAnswer>.addQuestionsToAnswers(): List<StudentAnswer> =
        withContext(Dispatchers.Default) {
            map { studentAnswer ->
                async {
                    studentAnswer
                        .copy(
                            questions = getQuestions(
                                studentAnswer.id
                                    ?: throw StudentAnswerErrors.NOT_FOUND
                            )
                        )
                }
            }.map { it.await() }
        }

    private suspend fun getStudentAnswerByKeys(studentId: Int, testId: Int): StudentAnswerDTO?  {
        return studentAnswerDao.getStudentAnswer(studentId, testId).await()
    }

    private suspend fun createQuestionAnswers(
        studentAnswer: StudentAnswer,
        studentAnswerId: Int,
    ) = withContext(Dispatchers.Default) {
        studentAnswer.questions?.forEach {
            launch {
                val questionAnswerDTO = it.toQuestionAnswerDTO(studentAnswerId)
                questionAnswerDao.create(questionAnswerDTO).await()
            }
        } ?: throw StudentAnswerErrors.NO_QUESTIONS
    }

    suspend fun createStudentAnswer(studentId: Int, answer: StudentAnswer): StudentAnswer {
        checkStudent(studentId)
        val answerDTO = answer.toStudentAnswerDTO()
        val checkStudentAnswer = getStudentAnswerByKeys(studentId, answer.testId)
        val studentAnswerId = if (checkStudentAnswer != null) {
            val stAnswerid = checkStudentAnswer.id ?: throw StudentAnswerErrors.NOT_FOUND
            questionAnswerDao.deleteByStudentAnswer(stAnswerid).await()
            studentAnswerDao.changeStudentAnswerById(stAnswerid,answer.toStudentAnswerDTO()).await()
            createQuestionAnswers(
                studentAnswer = answer,
                studentAnswerId = stAnswerid
            )
            stAnswerid
        } else {
            val stAnswerid = studentAnswerDao.create(answerDTO).await()
            createQuestionAnswers(answer, stAnswerid)
            stAnswerid
        }

        return getStudentAnswer(studentAnswerId)
    }

    suspend fun getStudentAnswer(studentAnswerId: Int): StudentAnswer {
        val studentAnswer = studentAnswerDao.getStudentAnswerById(studentAnswerId).await()
        val questions = getQuestions(studentAnswerId)
        return studentAnswer.copy(questions = questions)
    }

    suspend fun getStudentAnswersByStudentId(studentId: Int): List<StudentAnswer> {
        checkStudent(studentId)
        val studentAnswerList = studentAnswerDao.getStudentAnswersByStudentId(studentId).await()
        return studentAnswerList.addQuestionsToAnswers()
    }

    suspend fun getStudentsAnswersByGroup(groupId: Int, testId: Int): List<StudentAnswer> {
        val groupName = groupDao.findById(groupId).await().name
        checkTest(testId)
        val students = studentDao.getAllStudentByGroupName(groupName).await().map { it.studentId }
        return withContext(Dispatchers.Default) {
            students.map { studentId ->
                async { getStudentAnswersByStudentId(studentId) }
            }.map { it.await() }
                .reduce { a, b -> a.plus(b) }
        }
    }

    suspend fun checkTeacherAccessToGroup(teacherId: Int, groupId: Int) {
        teacherGroupDao.findByPrimaryKey(teacherId, groupId).await()
    }


}



