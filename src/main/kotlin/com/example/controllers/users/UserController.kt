package com.example.controllers.users

import at.favre.lib.crypto.bcrypt.BCrypt
import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.student.Students
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.user.UserDTO
import com.example.database.entity.user.Users
import com.example.errors.data.BasicErrors
import com.example.errors.data.StudentErrors
import com.example.errors.data.TeacherErrors
import com.example.errors.data.UserErrors
import com.example.middleware.ROLES
import com.example.middleware.TokenProvider
import com.example.models.auth.LoginData
import com.example.models.auth.TokenBody

class UserController(
    private val userDao:Users,
    private val tokenProvider:TokenProvider,
    private val teacherDao:Teachers,
    private val studentDao:Students,
): BaseController<UserDTO>() {

    override val dao: BaseEntity<UserDTO> = userDao

    private fun checkEmpty(dto:UserDTO){
        if (dto.login.isEmpty() || dto.password.isEmpty())
            throw BasicErrors.EMPTY_FIELDS
    }

    private suspend fun checkUser(userId: Int){
        userDao.findById(userId).await()
    }

    private fun checkPassword(db_password:String,login_password:String){
        val verified = BCrypt.verifyer().verify(login_password.toCharArray(),db_password.toCharArray()).verified
        if (!verified)
            throw UserErrors.PASSWORD
    }

    private fun checkRole(role:String){
        val roleList = ROLES.values().map { it.name }
        if (role !in roleList)
            throw UserErrors.ROLE
    }

    private suspend fun createToken(userId: Int,role: ROLES):String{
        return when(role){
            ROLES.ADMIN -> tokenProvider.createAdminToken(userId)
            ROLES.TEACHER -> {
                val teacherId = teacherDao.findByUserId(userId).await().teacherId
                tokenProvider.createTeacherToken(userId, teacherId)
            }
            ROLES.STUDENT -> {
                val studentId = studentDao.findByUserId(userId).await().studentId
                tokenProvider.createStudentToken(userId, studentId)
            }
            ROLES.GUEST -> tokenProvider.createGuestToken(userId)
        }
    }

    private fun hashPassword(password: String):String = BCrypt.withDefaults().hashToString(4,password.toCharArray())

    suspend fun registerUser(dto: UserDTO): Int {
        checkEmpty(dto)
        userDao.checkLogin(dto.login)
        checkRole(dto.role)
        val user = dto.copy(password = hashPassword(dto.password))
        return userDao.create(user).await()
    }

    suspend fun login(loginData: LoginData): TokenBody {
        val user = userDao.findByLogin(loginData.login).await() ?: throw UserErrors.NOT_FOUND
        checkPassword(user.password,loginData.password)
        val userId = user.id ?: throw UserErrors.NOT_FOUND
        val role = ROLES.valueOf(user.role)
        val token = createToken(userId,role)
        return TokenBody(token,role.name)
    }

    suspend fun changePassword(userId:Int,newPassword:String){
        checkUser(userId)
        val password = hashPassword(newPassword)
        userDao.changePassword(userId,password).await()
    }

    suspend fun changeLogin(userId:Int,newLogin:String){
        checkUser(userId)
        userDao.checkLogin(newLogin)
        userDao.changeLogin(userId,newLogin).await()
    }

    suspend fun changeUserRole(userId: Int,role:String){
        checkUser(userId)
        checkRole(role)
        val userRole = ROLES.valueOf(role)
        userDao.changeRole(userId,userRole).await()
    }

    suspend fun changeFio(userId: Int,fio:String){
        checkUser(userId)
        userDao.changeFio(userId,fio).await()
    }


}


