package com.example.controllers.teacher

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.group.Groups
import com.example.database.entity.student.Students
import com.example.database.entity.teacher.TeacherDTO
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.user.Users
import com.example.errors.data.GroupErrors
import com.example.errors.data.StudentErrors
import com.example.middleware.ROLES
import com.example.models.teacher.TeacherInfo
import com.example.models.teacher.TeacherInfoWithGroupList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class TeacherController(
    private val teacherDao:Teachers,
    private val userDao:Users,
    private val groupDao:Groups,
    private val studentDao:Students,
):BaseController<TeacherDTO>() {

    override val dao:BaseEntity<TeacherDTO> = teacherDao

    private suspend fun checkUser(userId: Int){
        userDao.findById(userId).await()
    }

    private suspend fun checkGroup(groupId: Int){
        groupDao.findById(groupId).await()
    }

    private suspend fun checkStudent(studentId: Int){
        studentDao.findById(studentId).await()
    }

    private suspend fun checkTeacher(teacherId: Int){
        findById(teacherId)
    }

    suspend fun registerTeacher(dto:TeacherDTO):Int{
        teacherDao.checkUserNotTeacher(dto.userId)
        return teacherDao.create(dto).await()
    }

    suspend fun teacherInfo(teacherId: Int):TeacherInfo{
        checkTeacher(teacherId)
        return teacherDao.getTeacher(teacherId).await()
    }

    suspend fun groupTeachersInfo(groupId:Int):List<TeacherInfo>{
        checkGroup(groupId)
        return teacherDao.getGroupTeachers(groupId).await()
    }

    suspend fun getAllTeachersInfo():List<TeacherInfo>{
        return teacherDao.getAllTeacherInfo().await()
    }

    suspend fun getAllWithGroup():List<TeacherInfoWithGroupList>{
        val teacherInfoList = teacherDao.getAllTeacherInfo().await()
        return withContext(Dispatchers.Default){
            teacherInfoList.map { info ->
                async{
                    val teacherGroups = groupDao.getTeacherGroups(info.teacherId).await()
                    TeacherInfoWithGroupList.fromParts(teacherInfo = info, groups = teacherGroups)
                }
            }.map { it.await() }
        }
    }

    suspend fun getTeachersInfoWithGroup(groupId: Int):List<TeacherInfoWithGroupList>{
        checkGroup(groupId)
        val teacherList = teacherDao.getGroupTeachers(groupId).await()
        return withContext(Dispatchers.IO){
            teacherList.map { teacher ->
                async {
                    val groupList = groupDao.getTeacherGroups(teacher.teacherId).await()
                    TeacherInfoWithGroupList.fromParts(teacher,groupList)
                }
            }.map { it.await() }
        }
    }

    suspend fun getStudentTeachers(studentId:Int):List<TeacherInfo>{
        checkStudent(studentId)
        val groupName = studentDao.findById(studentId).await().groupName ?: throw StudentErrors.NOT_FOUND
        val groupId = groupDao.findGroupByName(groupName).await()?.id ?: throw GroupErrors.NOT_FOUND
        return groupTeachersInfo(groupId = groupId)
    }

    suspend fun deleteTeacher(teacherId: Int){
        val teacher = findById(teacherId)
        userDao.changeRole(teacher.userId,ROLES.GUEST).await()
        teacherDao.delete(teacherId).await()
    }

}