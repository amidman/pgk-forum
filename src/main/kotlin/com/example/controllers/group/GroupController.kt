package com.example.controllers.group

import com.example.controllers.BaseController
import com.example.database.BaseEntity
import com.example.database.entity.group.GroupDTO
import com.example.database.entity.group.Groups
import com.example.database.entity.student.Students
import com.example.database.entity.teacher.Teachers
import com.example.database.entity.teacher_group.TeacherGroups
import com.example.database.entity.user.Users
import com.example.errors.data.GroupErrors
import com.example.errors.data.StudentErrors
import com.example.errors.data.TeacherErrors
import com.example.errors.data.TeacherGroupErrors
import com.example.models.group.GroupInfo
import com.example.models.group.GroupInfoWithTeachersGroup
import com.example.models.group.TeacherInfoWithGroupDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class GroupController(
    private val groupDao:Groups,
    private val teacherGroupDao:TeacherGroups,
    private val teacherDao:Teachers,
    private val studentDao:Students,
    private val userDao:Users,
):BaseController<GroupDTO>() {

    override val dao: BaseEntity<GroupDTO> = groupDao

    suspend fun checkGroup(groupName:String){
        groupDao.findGroupByName(groupName).await() ?: throw GroupErrors.NOT_FOUND
    }

    private suspend fun checkTeacherGroup(teacherId: Int? = null,groupId: Int? = null){
        teacherId?.let { teacherDao.findById(teacherId).await() }
        groupId?.let { groupDao.findById(groupId).await() }
    }
    private suspend fun checkGroupTeacherNotExist(teacherId: Int, groupId: Int){
        checkTeacherGroup(teacherId, groupId)
        teacherGroupDao.findByPrimaryKey(teacherId,groupId).await()?.let { throw TeacherGroupErrors.EXIST }
    }

    private suspend fun checkGroupTeacherExist(teacherId: Int,groupId: Int){
        checkTeacherGroup(teacherId, groupId)
        teacherGroupDao.findByPrimaryKey(teacherId,groupId).await() ?: throw TeacherGroupErrors.NOT_FOUND
    }

    suspend fun createGroup(groupDTO: GroupDTO): Int {
        groupDao.checkGroupName(groupDTO.name)
        return groupDao.create(groupDTO).await()
    }

    suspend fun addGroupToTeacher(teacherLogin: String, groupName:String):TeacherInfoWithGroupDTO{
        val groupDTO = groupDao.findGroupByName(groupName).await() ?: throw GroupErrors.NOT_FOUND
        val groupId = groupDTO.id ?: throw GroupErrors.NOT_FOUND
        val userId = userDao.findByLogin(teacherLogin).await()?.id ?: throw TeacherErrors.NOT_FOUND
        val teacherInfo = teacherDao.findByUserId(userId).await()
        val teacherId = teacherInfo.teacherId
        checkGroupTeacherNotExist(groupId=groupId,teacherId=teacherId)
        teacherGroupDao.create(teacherId,groupId).await()
        return TeacherInfoWithGroupDTO(
            teacherInfo = teacherInfo,
            groupDTO = groupDTO
        )
    }

    suspend fun removeGroupFromTeacher(groupId: Int,teacherLogin: String){
        checkTeacherGroup(groupId = groupId)
        val userId = userDao.findByLogin(teacherLogin).await()?.id ?: throw TeacherErrors.NOT_FOUND
        val teacherId = teacherDao.findByUserId(userId).await().teacherId
        teacherGroupDao.delete(teacherId, groupId).await()
    }

    suspend fun createGroupWithTeacher(teacherId:Int,groupDTO: GroupDTO):Int{
        checkTeacherGroup(teacherId)
        val groupId = createGroup(groupDTO)
        teacherGroupDao.create(teacherId,groupId).await()
        return groupId
    }




    suspend fun groupInfo(groupId: Int):GroupInfo{
        checkTeacherGroup(groupId = groupId)
        val teacherInfoList = teacherDao.getGroupTeachers(groupId)
        val groupDTO = groupDao.findById(groupId)
        val groupStudents = studentDao.getAllStudentByGroupName(groupDTO.await().name)
        return GroupInfo.getFromParts(
            teacher = teacherInfoList.await(), groupDTO = groupDTO.await(), students = groupStudents.await()
        )
    }

    suspend fun groupInfoByName(groupName:String):GroupInfo{
        val groupId = groupDao.findGroupByName(groupName).await()?.id ?: throw GroupErrors.NOT_FOUND
        return groupInfo(groupId)
    }

    suspend fun studentGroupInfo(groupName: String?):GroupInfo {
        return groupName?.let {
            groupInfoByName(groupName)
        } ?: throw StudentErrors.GROUP_NULL
    }

    suspend fun getAllGroupInfo():List<GroupInfo> = withContext(Dispatchers.Default){
        val groupList = getAll().filter { it.id != null }
        val groupInfoList = groupList.map {
            async { groupInfo(it.id ?: throw GroupErrors.NOT_FOUND) }
        }.map { it.await() }
        groupInfoList
    }

    suspend fun getGroupsInfoByTeacher(teacherId: Int):List<GroupInfo> = withContext(Dispatchers.Default){
        checkTeacherGroup(teacherId=teacherId)
        val groupsId = teacherGroupDao.getGroupsIdByTeacher(teacherId).await()
        val groupInfoList = groupsId.map {
            async { groupInfo(it) }
        }.map { it.await() }
        groupInfoList
    }

    suspend fun changeGroupName(groupId: Int,newName:String){
        groupDao.checkGroupName(newName)
        checkTeacherGroup(groupId = groupId)
        groupDao.changeName(groupId,newName).await()
    }

    suspend fun changeShortName(groupId:Int,newShortName:String){
        checkTeacherGroup(groupId = groupId)
        groupDao.changeShortName(groupId,newShortName).await()
    }




}