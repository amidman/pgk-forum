package com.example

import com.example.database.DataBase
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.cio.*
import com.example.plugins.*
import com.example.routing.configureRouting
import com.example.validation.configureValidation
import com.typesafe.config.ConfigFactory
import io.ktor.server.config.*
import io.ktor.server.config.yaml.*

fun main() {
    embeddedServer(
        CIO,
        environment = applicationEngineEnvironment {
            val config = YamlConfig("application.yaml")?.config("application") ?: HoconApplicationConfig(ConfigFactory.load())
            this@applicationEngineEnvironment.config = config
            val host = config.property("ktor.deployment.host").getString()
            val port = config.property("ktor.deployment.port").getString().toInt()
            module {
                module()
            }
            connector{
                this.host = host
                this.port = port
            }
        }

    )
        .start(wait = true)
}

fun Application.module() {
    DataBase(
        config = environment.config
    ).init()
    configureKoinDI()
    configureValidation()
    configureStatusPages()
    configureSerialization()
    configureSecurity()
    configureRouting()
}
