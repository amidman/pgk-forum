val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val koin_version: String by project
val exposed_version: String by project
val mysql_connector_version: String by project
val postgre_connector_version: String by project


plugins {
    application
    kotlin("jvm") version "1.7.20"
    kotlin("plugin.serialization") version "1.7.20"
    id("io.ktor.plugin") version "2.1.3"
}

group = "com.example"
version = "0.0.1"
application {
    mainClass.set("com.example.ApplicationKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-auth-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-auth-jwt-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-cio-jvm:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation("io.ktor:ktor-server-tests-jvm:$ktor_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")

    //kotlin serialization
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktor_version")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")

    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")

    //sql connector
    implementation("org.postgresql:postgresql:$postgre_connector_version")

    //hikari
    implementation("com.zaxxer:HikariCP:3.4.2")

    //StatusPages
    implementation("io.ktor:ktor-server-status-pages:$ktor_version")

    // Koin
    implementation("io.insert-koin:koin-ktor:$koin_version")

    //Password Encryption
    implementation("at.favre.lib:bcrypt:0.9.0")

    // Request Validation
    implementation("io.ktor:ktor-server-request-validation:$ktor_version")

    //kotlinx localdatetime
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")

    //
    implementation("io.ktor:ktor-server-config-yaml:$ktor_version")

}